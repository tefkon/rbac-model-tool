package ee.ut.cs.sep.rbacmodeltool.model.rbac;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class RbacModel {
	
	/**
	 * TODO: in the future identify elements by their ID {@link ElementID}
	 */
	public HashMap<String, Subject> subjects = new HashMap<String, Subject>();
	
	public HashMap<String, Role> roles = new HashMap<String, Role>();
	
	public HashMap<String, ResourceAttribute> resourceAttributes = new HashMap<String, ResourceAttribute>();
	
	public HashMap<String, Operation> operations = new HashMap<String, Operation>();
	
	// This is crap.. don't use this!
	//public PermissionList permissionList = new PermissionList(this);
	
	//public List<Permission> permissions = new ArrayList<Permission>();
	
	public HashMap<String, Permission> permissions = new HashMap<String, Permission>();
	
	public HashMap<ResourceAttribute, HashMap<Operation, Permission>> permissionMapper = 
		new HashMap<ResourceAttribute, HashMap<Operation, Permission>>();
	
	public Permission createOrGetPermission(ResourceAttribute resource, Operation operation) {
		Permission permission = null;
		if (permissionMapper.containsKey(resource) &&
			permissionMapper.get(resource).containsKey(operation)) {
			
			return permissionMapper.get(resource).get(operation);
		} else {
			permission = new Permission(this, operation, resource);
		}
		
		addPermission(permission);
		
		return permission;
	}
	
	public Permission getPermission(String identificator) {
		
		return permissions.get(identificator);
	}
	
	public Permission getPermission(ResourceAttribute resource, Operation operation) {
		Permission permission = null;
		if (permissionMapper.containsKey(resource) &&
			permissionMapper.get(resource).containsKey(operation)) {
			
			permission = permissionMapper.get(resource).get(operation);
		}
		
		return permission;
	}
	
	public RbacModel addPermission(Permission permission) {
		HashMap<Operation, Permission> operationPermissionMapper = new HashMap<Operation, Permission>();
		operationPermissionMapper.put(permission.getOperation(), permission);
		if (permissionMapper.containsKey(permission.getResource())) {
			operationPermissionMapper = permissionMapper.get(permission.getResource());
		} else {
			permissions.put(permission.getName(), permission);
		}
		operationPermissionMapper.put(permission.getOperation(), permission);
		permissionMapper.put(permission.getResource(), operationPermissionMapper);
		
		return this;
	}
	
	public Subject createOrGetSubject(String name) {
		Subject subject = this.getSubject(name);
		if (subject != null) {
			
			return subject;
		}
		
		return new Subject(name, this);
	}
	
	public Role createOrGetRole(String name) {
		Role role = this.getRole(name);
		if (role != null) {
			
			return role;
		}
		
		return new Role(name, this);
	}
	
	public Operation createOrGetOperation(String name) {
		Operation operation = this.getOperation(name);
		if (operation != null) {
			
			return operation;
		}
		
		return new Operation(name, this);
	}
	
	public ResourceAttribute createOrGetResourceAttribute(String name) {
		ResourceAttribute resource = this.getResourceAttribute(name);
		if (resource != null) {
			
			return resource;
		}
		
		return new ResourceAttribute(name, this);
	}
	
	public Subject getSubject(String name) {
		
		return this.subjects.get(name);
	}
	
	public RbacModel addSubject(Subject subject) {
		this.subjects.put(subject.name, subject);
		
		return this;
	}
	
	public Role getRole(String name) {
		
		return this.roles.get(name);
	}
	
	public RbacModel addRole(Role role) {
		this.roles.put(role.name, role);
		
		return this;
	}
	
	public ResourceAttribute getResourceAttribute(String name) {
		
		return resourceAttributes.get(name);
	}
	
	public RbacModel addResourceAttribute(ResourceAttribute attribute) {
		this.resourceAttributes.put(attribute.name, attribute);
		
		return this;
	}
	
	public Operation getOperation(String name) {
		
		return this.operations.get(name);
	}
	
	public RbacModel addTaskToResourceAttribute(Operation task, ResourceAttribute attribute) {
		attribute.addOperation(task);
		
		return this;
	}
	
	public RbacModel addValueToResourceAttribute(Object value, ResourceAttribute attribute) {
		attribute.addValue(value);
		
		return this;
	}
	
	public RbacModel addOperation(Operation operation) {
		this.operations.put(operation.name, operation);
		
		return this;
	}
	
	public boolean containsSubject(String name) {
		
		return this.subjects.containsKey(name);
	}
	
	public boolean containsRole(String name) {
		
		return this.roles.containsKey(name);
	}

	public boolean containsOperation(String name) {
		
		return this.operations.containsKey(name);
	}
	
	public boolean containsResourceAttribute(String name) {
		
		return this.resourceAttributes.containsKey(name);
	}
	
	public List<Subject> getSubjects() {
		List<Subject> result = new ArrayList<Subject>();
		for (Subject subject : this.subjects.values()) {
			result.add(subject);
		}
		
		return result;
	}
	
	public List<Role> getRoles() {
		List<Role> result = new ArrayList<Role>();
		for (Role role : this.roles.values()) {
			result.add(role);
		}
		
		return result;
	}
	
	public List<Operation> getOperations() {
		List<Operation> result = new ArrayList<Operation>();
		for (Operation operation : this.operations.values()) {
			result.add(operation);
		}
		
		return result;
	}

	public List<ResourceAttribute> getResourceAttributes() {
		List<ResourceAttribute> result = new ArrayList<ResourceAttribute>();
		for (ResourceAttribute attribute : this.resourceAttributes.values()) {
			result.add(attribute);
		}
		
		return result;
	}
	
	/**
	 * Compare by name (instances can be different)
	 * @param role
	 * @return
	 */
	public boolean hasRole(Role role) {
		
		return this.hasRole(role.name);
	}
	
	/**
	 * Compare by name (instances can be different)
	 * @param roleName
	 * @return
	 */
	public boolean hasRole(String roleName) {
		
		return this.roles.containsKey(roleName);
	}
	
	/**
	 * Compare by name (instances can be different)
	 * @param resource
	 * @return
	 */
	public boolean hasResourceAttribute(ResourceAttribute resource) {
	
		return this.hasResourceAttribute(resource.name);
	}
	
	/**
	 * Compare by name (instances can be different)
	 * @param resourceName
	 * @return
	 */
	public boolean hasResourceAttribute(String resourceName) {
		
		return this.resourceAttributes.containsKey(resourceName);
	}
	
	/**
	 * Compare by name (instances can be different)
	 * @param operationName
	 * @return
	 */
	public boolean hasOperation(Operation operation) {
		
		return this.hasOperation(operation.getName());
	}
	
	/**
	 * Compare by name (instances can be different)
	 * @param operationName
	 * @return
	 */
	public boolean hasOperation(String operationName) {
		
		return this.operations.containsKey(operationName);
	}
	
	public RbacModel merge(RbacModel other) {
		// TODO : remove old ones ?
		
		for (Role role : other.getRoles()) {
			addRole(role);
		}
		
		for (ResourceAttribute resourceAttribute : other.getResourceAttributes()) {
			addResourceAttribute(resourceAttribute);
		}
		
		for (Operation operation : other.getOperations()) {
			addOperation(operation);
		}
		
		for (Subject subject : other.getSubjects()) {
			addSubject(subject);
		}
		
	//	permissionList.merge(other.getPermissionList(), other);
		
		return this;
	}
	
	public HashMap<ResourceAttribute, HashMap<Operation, HashSet<Role>>> getRolePermissionAssignments() {
		// ALLOW
		// Resource - operation - roles ?
		
		// FIXME: at the moment role per operation, instead there can be
		// multiple roles per operation !
		HashMap<ResourceAttribute, HashMap<Operation, HashSet<Role>>> rolePermissionAssignments =
			new HashMap<ResourceAttribute, HashMap<Operation, HashSet<Role>>>();
		
		for (Role role : getRoles()) {
			for (Permission permission : role.getPermissions()) {
				HashMap<Operation, HashSet<Role>> opRole = new HashMap<Operation, HashSet<Role>>();
				if (rolePermissionAssignments.containsKey(permission.getResource())) {
					opRole = rolePermissionAssignments.get(permission.getResource());
				}
				
				if (opRole.containsKey(permission.getOperation())) {
					opRole.get(permission.getOperation()).add(role);
				} else {
					HashSet<Role> roles = new HashSet<Role>();
					roles.add(role);
					opRole.put(permission.getOperation(), roles);
				}
				
				rolePermissionAssignments.put(permission.getResource(), opRole);
				
				// Testing
//				if (role.getName().equals("A2_4")) {
//					System.out.println("A2_4:" + permission.getOperation().getName() + " " + permission.getResource().getName());
//				}
				
			}
		}
		
		return rolePermissionAssignments;
	}

	public void reset() {
		this.operations.clear();
		this.resourceAttributes.clear();
		this.roles.clear();
		this.subjects.clear();
		this.permissions.clear();
		this.permissionMapper.clear();
	}
}
