package ee.ut.cs.sep.rbacmodeltool.model.businessprocess;

import java.util.HashSet;
import java.util.Set;

/**
 * Organisation group
 */
public class BPGroup {

	private String name;
	private Set<BPOriginator> originators = new HashSet<BPOriginator>();
	
	public BPGroup(String name) {
		this.name = name;
	}
	
	public String getName() {
		
		return this.name;
	}
	
	public BPGroup addOriginator(BPOriginator originator) {
		this.originators.add(originator);
		
		return this;
	}
	
	public Set<BPOriginator> getOriginators() {
		
		return this.originators;
	}
}
