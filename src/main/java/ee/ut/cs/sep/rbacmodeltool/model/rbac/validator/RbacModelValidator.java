package ee.ut.cs.sep.rbacmodeltool.model.rbac.validator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

import ee.ut.cs.sep.rbacmodeltool.model.rbac.Operation;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.Permission;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.RbacModel;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.ResourceAttribute;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.Role;
import ee.ut.cs.sep.rbacmodeltool.util.StringUtil;

public class RbacModelValidator {

	private final String REASON_NEW_ROLE 		= "New role";
	private final String REASON_NEW_RESOURCE 	= "New resource";
	private final String REASON_NOT_ALLOWED 	= "Not allowed";
	private final String REASON_OUTDATED		= "Outdated";

	private RbacModel baseModel;
	private RbacModel model;

	private Set<String> newRoleNames = new HashSet<String>();
	private Set<String> newResourceAttributeNames = new HashSet<String>();

	public List<PermissionViolation> violations = new ArrayList<PermissionViolation>();
	public HashMap<String, PermissionViolation> violationMapping = new HashMap<String, PermissionViolation>();

	private boolean isValidated = false;
	private boolean isResolved = false;
	private boolean isValid = false;

	public RbacModelValidator(RbacModel baseModel, RbacModel model) {
		this.baseModel = baseModel;
		this.model = model;
	}
	
	public RbacModel getModel() {
		
		return this.model;
	}

	public boolean isValid() {
		if (this.isValidated) {

			return this.isValid;
		}

		this.isValidated = true;

		return this.validate();
	}

	/**
	 * Compare new model against old model. If there are discrepancies, they are
	 * added as violations.
	 */
	private boolean validate() {
		boolean isValid = true;

		// Check for new roles
		for (Role role : model.getRoles()) {
			if (!baseModel.hasRole(role.getName())) {
				newRoleNames.add(role.getName());
			}
		}

		for (ResourceAttribute resource : model.getResourceAttributes()) {
			if (!baseModel.hasResourceAttribute(resource.getName())) {
				newResourceAttributeNames.add(resource.getName());
			}
		}

		boolean isNewResource = false;
		boolean isNewRole = false;

		List<String> reasons = new ArrayList<String>();

		/**
		 * Comparing current model to base model. However the base model can
		 * contain outdated information, which needs to be eliminated that: 1)
		 * violations are resolved (in most cases new assignments or data are
		 * added); 2) outdated information removed.
		 * 
		 * Update (06.04.2014):
		 * Keep also old information that can be included/excluded afterwards.
		 */
		HashMap<ResourceAttribute, HashMap<Operation, HashSet<Role>>> rolePermissionAssignments = model
				.getRolePermissionAssignments();
		
		for (Entry<ResourceAttribute, HashMap<Operation, HashSet<Role>>> entry : rolePermissionAssignments
				.entrySet()) {

			String resourceName = entry.getKey().getName();
			if (newResourceAttributeNames.contains(resourceName)) {
				isNewResource = true;
				reasons.add(REASON_NEW_RESOURCE);
			}

			for (Entry<Operation, HashSet<Role>> subEntry : entry.getValue().entrySet()) {
				
				String operationName = subEntry.getKey().getName();
				
				for (Role role : subEntry.getValue()) {
					
					String roleName = role.getName();
					
					if (newRoleNames.contains(roleName)) {
						isNewRole = true;
						reasons.add(REASON_NEW_ROLE);
					}

					if (isNewResource || isNewRole) {
						String reason = StringUtil.concatenate(reasons);
						addViolation(roleName, operationName, resourceName, reason);
						reasons.remove(REASON_NEW_ROLE);
						isNewRole = false;
						continue;
					}
					
					ResourceAttribute baseResource = baseModel
							.getResourceAttribute(resourceName);

					Operation baseOperation = baseModel.getOperation(operationName);
					Role baseRole = baseModel.getRole(roleName);
					if (baseRole != null) {
						Permission permission = baseModel.getPermission(
								baseResource, baseOperation);
						if (baseResource == null) {
							//System.out.println("baseResource is null");
						}
						
						if (baseOperation == null) {
							//System.out.println("baseOp is null");
						}
						
						if (!baseRole.hasPermission(permission)) {
							addViolation(roleName, operationName, resourceName, REASON_NOT_ALLOWED);
							isValid = false;
						}
					}
				}

				
			}

			isNewResource = false;
			reasons.clear();
		}
		
		
		rolePermissionAssignments = baseModel.getRolePermissionAssignments();
		
		for (Entry<ResourceAttribute, HashMap<Operation, HashSet<Role>>> entry : rolePermissionAssignments
				.entrySet()) {
			
			ResourceAttribute baseResource = entry.getKey();
			
			for (Entry<Operation, HashSet<Role>> subEntry : entry.getValue().entrySet()) {
				
				Operation baseOperation = subEntry.getKey();
				Operation operation = model.getOperation(baseOperation.getName());
				
				for (Role baseRole : subEntry.getValue()) {
					Role role = model.getRole(baseRole.getName());
					ResourceAttribute resource = model.getResourceAttribute(baseResource.getName());
					
					// Role exists in newer RBAC model,
					// otherwise, role is new, i.e., should be already in new roles
					if (role != null) {
						Permission permission = model.getPermission(resource,
								operation);
						
						if (!role.hasPermission(permission)) {
							addViolation(baseRole, baseOperation, baseResource, REASON_OUTDATED);
							isValid = false;
						}
					}
				}
			}
		}

		this.isValid = isValid;

		return this.isValid;
	}

	public void resolve() {
		checkIfValidated();
		
		if (isResolved()) {
			return;
		}

		PermissionViolationResolver resolver = new PermissionViolationResolver(baseModel, model, violations);
		resolver.resolve();
		
		this.isResolved = resolver.isResolved();
	}
		
	public List<PermissionViolation> getViolations() {
		checkIfValidated();

		return this.violations;
	}

	public Set<String> getNewRoleNames() {

		return this.newRoleNames;
	}

	public Set<String> getNewResourceAttributeNames() {

		return this.newResourceAttributeNames;
	}
	
	protected boolean isResolved() {
		
		return isResolved;
	}

	protected void checkIfValidated() {
		if (!this.isValidated) {
			throw new RuntimeException("RBAC model needs to be validated first");
		}
	}
	
	protected PermissionViolation addViolation(Role role, Operation operation, ResourceAttribute resource, 
			 String reason) {

		return addViolation(role.getName(), operation.getName(), resource.getName(), reason);
	}
	
	protected PermissionViolation addViolation(String roleName, String operationName,
			String resourceName, String reason) {

		PermissionViolation permissionViolation = new PermissionViolation(
				roleName, operationName, resourceName, reason);

		this.violations.add(permissionViolation);
		
		if (violationAlreadyExists(roleName, operationName, resourceName, reason)) {
			permissionViolation.increaseNrOccurances();
		} else {
			addViolationMapping(roleName, operationName, resourceName, reason, permissionViolation);
		}
		
		return permissionViolation;
	}
	
	protected void addViolationMapping(ResourceAttribute resource, Role role,
			Operation operation, String reason, PermissionViolation violation) {

		addViolationMapping(role.getName(), operation.getName(), resource.getName(), reason, violation);
	}
	
	protected void addViolationMapping(String roleName, String operationName,
			String resourceName, String reason, PermissionViolation violation) {
		
		String key = generateKey(roleName, operationName, resourceName, reason);
		
		this.violationMapping.put(key, violation);
	}
	
	protected boolean violationAlreadyExists(String roleName, String operationName,
			String resourceName, String reason) {
		
		String key = generateKey(roleName, operationName, resourceName, reason);
		
		return (this.violationMapping.containsKey(key));
	}
	
	protected String generateKey(String roleName, String operationName,
			String resourceName, String reason) {
		
		return (roleName + operationName + resourceName + reason);
	}
	
	public void reset() {
		this.violations.clear();
		this.isValid = false;
		this.isValidated = false;
		this.newResourceAttributeNames.clear();
		this.newRoleNames.clear();
	}
}
