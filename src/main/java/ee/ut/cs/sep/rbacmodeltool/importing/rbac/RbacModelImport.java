package ee.ut.cs.sep.rbacmodeltool.importing.rbac;

import java.io.File;
import javax.swing.filechooser.FileFilter;
import ee.ut.cs.sep.rbacmodeltool.importing.AbstractImport;
import ee.ut.cs.sep.rbacmodeltool.io.rbac.RbacModelFile;

public class RbacModelImport extends AbstractImport {

	public FileFilter getFileFilter() {
		if (fileFilter == null) {
			fileFilter = new FileFilter() {
				public boolean accept(File file) {
					boolean valid = file.getName().endsWith(".xml");
					
					return file.isDirectory() || valid && file.isFile();
				}
				
				public String getDescription() {
					return "XML (*.xml)";
				}
			};
		}
		
		return fileFilter;
	}

	public void importFile(File file) {
		this.file = new RbacModelFile(file);
	}
}
