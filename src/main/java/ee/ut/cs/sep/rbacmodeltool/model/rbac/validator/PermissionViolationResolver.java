package ee.ut.cs.sep.rbacmodeltool.model.rbac.validator;

import java.util.ArrayList;
import java.util.List;

import ee.ut.cs.sep.rbacmodeltool.model.rbac.Operation;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.Permission;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.RbacModel;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.ResourceAttribute;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.Role;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.Subject;

public class PermissionViolationResolver {
	
	protected RbacModel baseModel;
	protected RbacModel model;
	protected List<PermissionViolation> violations = new ArrayList<PermissionViolation>();
	protected boolean isResolved = false;
	
	public PermissionViolationResolver(RbacModel baseModel, RbacModel model,
		List<PermissionViolation> violations) {
		
		this.baseModel = baseModel;
		this.model = model;
		this.violations = violations;
	}

	public boolean isResolved() {
		
		return this.isResolved;
	}
	
	public void resolve() {
		if (isResolved()) {
			return;
		}
		
		for (PermissionViolation violation : this.violations) {
			if (violation.isKeep()) {
				Role role = resolveRole(violation.getRoleName());
				Operation operation = resolveOperation(violation.getOperationName());
				ResourceAttribute resource = resolveResourceAttribute(violation.getResourceName());
				resource.addOperation(operation);
				Permission permission = model.createOrGetPermission(resource, operation);
				role.addPermission(permission);
				model.addPermission(permission);
			}
		}
		
		this.isResolved = true;
	}
	
	protected Role resolveRole(String roleName) {
		Role role = model.createOrGetRole(roleName);
		Role baseRole = baseModel.getRole(roleName);
		if (baseRole != null) {
			for (Subject baseSubject : baseRole.getSubjects()) {
				Subject subject = model.createOrGetSubject(baseSubject.getName());
				role.addSubject(subject);
				model.addSubject(subject);
			}
		}
		
		model.addRole(role);
		
		return role;
	}
	
	protected Operation resolveOperation(String operationName) {
		Operation operation = model.getOperation(operationName);
		if (operation == null) {
			operation = model.createOrGetOperation(operationName);
		}
		
		model.addOperation(operation);
		
		return operation;
	}
	
	protected ResourceAttribute resolveResourceAttribute(String resourceName) {
		ResourceAttribute resource = model.createOrGetResourceAttribute(resourceName);
		ResourceAttribute baseResource = baseModel.getResourceAttribute(resourceName);
		if (baseResource != null) {
			for (Object value : baseResource.getValues()) {
				resource.addValue(value);
			}
			
			for (Operation baseOperation : baseResource.getOperations()) {
				Operation operation = model.createOrGetOperation(baseOperation.getName());
				resource.addOperation(operation);
				model.addOperation(operation);
			}
		}
		
		model.addResourceAttribute(resource);
		
		return resource;
	}
	
}
