package ee.ut.cs.sep.rbacmodeltool.gui.tablemodel;

import javax.swing.table.AbstractTableModel;

import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.BPAttribute;

@SuppressWarnings("serial")
public class AttributeOperationsTableModel extends AbstractTableModel {

	public static final String[] columnNames = { "Operations" };
	
	private BPAttribute selectedAttribute = null;
	
	public int getColumnCount() {
		
		return columnNames.length;
	}

	public int getRowCount() {
		if (selectedAttribute == null) {
			return 0;
		}
		
		return selectedAttribute.getActivities().size();
	}

	public Object getValueAt(int row, int col) {
		if (selectedAttribute == null) {
			return null;
		}

		return selectedAttribute.getActivities().get(row).getName();
	}
	
	public void setValueAt(Object value, int row, int col) {
		fireTableCellUpdated(row, col);
	}
	
	public String getColumnName(int col) {
		
		return columnNames[col];
	}

	public boolean isCellEditable(int row, int col) {

		return false;
	}
	
	public Class<?> getColumnClass(int col) {
		
		return String.class;
	}
	
	public void updateTable() {
		this.selectedAttribute = null;
		super.fireTableDataChanged();
	}
	
	public void updateTable(BPAttribute attribute) {
		this.selectedAttribute = attribute;
		
		super.fireTableDataChanged();
	}
}
