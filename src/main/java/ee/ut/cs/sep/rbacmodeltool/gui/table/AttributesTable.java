package ee.ut.cs.sep.rbacmodeltool.gui.table;

import java.awt.Dimension;

import javax.swing.JTable;

import ee.ut.cs.sep.rbacmodeltool.gui.tablemodel.AttributesTableModel;

@SuppressWarnings("serial")
public class AttributesTable extends JTable {

	public AttributesTable(AttributesTableModel attributesTableModel) {
		this.setPreferredScrollableViewportSize(new Dimension(180, 70));
        this.setFillsViewportHeight(true);
        this.setModel(attributesTableModel);
	}
}
