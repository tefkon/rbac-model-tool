package ee.ut.cs.sep.rbacmodeltool.gui;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import ee.ut.cs.sep.rbacmodeltool.ViewModel;
import ee.ut.cs.sep.rbacmodeltool.StateEnum;
import ee.ut.cs.sep.rbacmodeltool.gui.table.AttributeOperationsTable;
import ee.ut.cs.sep.rbacmodeltool.gui.table.AttributesTable;
import ee.ut.cs.sep.rbacmodeltool.gui.table.OperationRolesTable;
import ee.ut.cs.sep.rbacmodeltool.gui.table.OperationsTable;
import ee.ut.cs.sep.rbacmodeltool.gui.table.RolesTable;
import ee.ut.cs.sep.rbacmodeltool.gui.tablemodel.AttributeOperationsTableModel;
import ee.ut.cs.sep.rbacmodeltool.gui.tablemodel.AttributesTableModel;
import ee.ut.cs.sep.rbacmodeltool.gui.tablemodel.OperationRolesTableModel;
import ee.ut.cs.sep.rbacmodeltool.gui.tablemodel.OperationsTableModel;
import ee.ut.cs.sep.rbacmodeltool.gui.tablemodel.PermissionViolationTableModel;
import ee.ut.cs.sep.rbacmodeltool.gui.tablemodel.RolesTableModel;
import ee.ut.cs.sep.rbacmodeltool.importing.businessprocess.XesImport;
import ee.ut.cs.sep.rbacmodeltool.importing.rbac.RbacModelImport;
import ee.ut.cs.sep.rbacmodeltool.io.ltl.LTLFile;
import ee.ut.cs.sep.rbacmodeltool.io.rbac.RbacModelFile;
import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.BPActivity;
import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.BPAttribute;
import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.BPOriginator;
import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.ProcessModel;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.algorithms.RbacModelXmlWriter;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.validator.RbacModelValidator;
import ee.ut.cs.sep.rbacmodeltool.processmining.ltl.algorithms.LTLWriter;

@SuppressWarnings("serial")
public class MainFrame extends JFrame implements ActionListener {
	
	private static int INDEX_TOP_PANEL 			= 0;
	private static int INDEX_INFO_PANEL 		= 1;
	private static int INDEX_PANE 				= 2;
	
	private static int MENU_OPEN_LOG 			= 0;
	private static int MENU_IMPORT_RBAC_MODEL 	= 1;
	private static int MENU_EXPORT_RBAC_MODEL	= 2;
	private static int MENU_EXPORT_LTL_RULES 	= 3;
	
	private JFileChooser fileChooser;
	private JMenu menu;
	private JTable rolesTable;
	private JTable attributesTable;
	private JTable tasksTable;
	private JTable attributesOperationsTable;
	private JTable operationRolesTable;
	private JTable permissionViolationsTable;
	private JList<String> subjectList;
	private JButton analyzeButton;
	private JButton createRbacModelButton;
	private JButton validateButton;
	private JButton exportRbacModelButton;
	private JButton resetButton;
	private JButton mergeExportButton;
	private String[] subjects = new String[]{};
	
	private JDialog validationDialog = new JDialog(this);
	private MessageWriter messageWriter;
	
	/* XES import */
	private XesImport xesImport = new XesImport();
	/* RBAC model import */
	private RbacModelImport rbacModelImport = new RbacModelImport();
	/* Intermediary for models and visualization */
	private ViewModel viewModel;
	
	public MainFrame(ViewModel viewModel) {
		this.viewModel = viewModel;
		viewModel.setView(this);
	}
	
	public void initialize() {
		initTables();
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Could not load Look And Feel",
				"Invalid LookandFeel", JOptionPane.ERROR_MESSAGE);
		}
		
		initButtons();
		initRbacModelViolationsDialog();
		
		this.setJMenuBar(createMenuBar());
		this.add(createMainPanel());
		
		updateVisualizations();
	
	}
	
	protected JPanel createMainPanel() {
		 /** Subjects */
		subjectList = new JList<String>();
        subjectList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        subjectList.setVisibleRowCount(-1);
        subjectList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting() &&
					subjectList.getSelectedValue() != null) {
					BPOriginator originator = viewModel.getProcessModel().getOriginator(subjectList.getSelectedValue());
					((RolesTableModel) rolesTable.getModel()).updateTable(originator);
				}
			}
		});
        
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        JPanel contentPanel = createContentPanel();
        JPanel buttonsPanel = createButtonsPanel();
        mainPanel.add(buttonsPanel, BorderLayout.SOUTH, INDEX_TOP_PANEL);
        mainPanel.add(contentPanel, BorderLayout.CENTER, INDEX_INFO_PANEL);
        mainPanel.add(createMessagesScrollPane(), BorderLayout.CENTER, INDEX_PANE);
		mainPanel.setMaximumSize(new Dimension(700, 400));
		
		return mainPanel;
	}
	
	protected JScrollPane createMessagesScrollPane() {
		JTextPane messagesPane = new JTextPane();
		messagesPane.setFont(new Font("Courier New", Font.PLAIN, 12));
		messagesPane.setEditable(false);
		
		messageWriter = new MessageWriter(messagesPane);
		
		JScrollPane messagesScrollPane = new JScrollPane(messagesPane);
		messagesScrollPane.setPreferredSize(new Dimension(900, 100));

		return messagesScrollPane;
	}
	
	protected JPanel createContentPanel() {
		JTabbedPane tabbedPane = new JTabbedPane();
		getContentPane().add(tabbedPane);

		tabbedPane.addTab("Roles", createRoleToSubjectsPanel());
        tabbedPane.addTab("Attributes", createAttributesPanel());
        tabbedPane.addTab("Operations", createOperationsPanel());
        
		JPanel contentPanel = new JPanel(new BorderLayout());
		contentPanel.add(tabbedPane, BorderLayout.CENTER);

		return contentPanel;
	}
	
	protected JPanel createRoleToSubjectsPanel() {
		JScrollPane subjectListScrollPane = new JScrollPane(subjectList);
		subjectListScrollPane.setMinimumSize(new Dimension(200, 20));
		subjectListScrollPane.setAlignmentX(LEFT_ALIGNMENT);

		JScrollPane rolesTableScrollPane = new JScrollPane(rolesTable);
		rolesTableScrollPane.setAutoscrolls(true);
		rolesTableScrollPane.setMinimumSize(new Dimension(200, 20));
		rolesTableScrollPane.setAlignmentX(LEFT_ALIGNMENT);

        JButton addRoleButton = new JButton("Add role");
        addRoleButton.setActionCommand(ActionCommand.ADD_ROLE);
        addRoleButton.addActionListener(this);
        addRoleButton.setPreferredSize(new Dimension(100, 25));
        
        JButton removeRoleButton = new JButton("Remove");
        removeRoleButton.setActionCommand(ActionCommand.REMOVE_ROLE);
        removeRoleButton.addActionListener(this);
        removeRoleButton.setPreferredSize(new Dimension(100, 25));
        
        JPanel roleToSubjectButtonsPanel = new JPanel();
        roleToSubjectButtonsPanel.add(addRoleButton);
        roleToSubjectButtonsPanel.add(removeRoleButton);
        
        JPanel roleToSubjectPanel = new JPanel(new BorderLayout(5, 5));
        Border roleToSubjectBorder = BorderFactory.createTitledBorder("Roles");
        roleToSubjectPanel.setBorder(roleToSubjectBorder);

		JSplitPane subjectToRolesSplitPane = new JSplitPane(
			JSplitPane.HORIZONTAL_SPLIT, subjectListScrollPane,
			rolesTableScrollPane);
    	
        roleToSubjectPanel.add(subjectToRolesSplitPane, BorderLayout.CENTER);
		roleToSubjectPanel.add(roleToSubjectButtonsPanel, BorderLayout.SOUTH);
		
        return roleToSubjectPanel;
	}
	
	protected JPanel createAttributesPanel() {
		JScrollPane attributeOperationsTableScrollPane = new JScrollPane(
				attributesOperationsTable);
		attributeOperationsTableScrollPane.setAutoscrolls(true);
		attributeOperationsTableScrollPane.setPreferredSize(new Dimension(270,
				70));
		attributeOperationsTableScrollPane.setAlignmentX(LEFT_ALIGNMENT);

		JScrollPane attributesTableScrollPane = new JScrollPane(attributesTable);
		attributesTableScrollPane.setAutoscrolls(true);
		attributesTableScrollPane.setPreferredSize(new Dimension(270, 70));
		attributesTableScrollPane.setAlignmentX(LEFT_ALIGNMENT);

	
		JSplitPane attributesOperationsSplitPane = new JSplitPane(
				JSplitPane.HORIZONTAL_SPLIT, attributesTableScrollPane,
				attributeOperationsTableScrollPane);

		JPanel attributesPanel = new JPanel(new BorderLayout());
		attributesPanel.setBorder(BorderFactory
				.createTitledBorder("Attributes"));
		attributesPanel.add(attributesOperationsSplitPane, BorderLayout.CENTER);  
		
		return attributesPanel;
	}
	
	protected JPanel createOperationsPanel() {
		JScrollPane operationsTableScrollPane = new JScrollPane(tasksTable);
		operationsTableScrollPane.setAutoscrolls(true);
		operationsTableScrollPane.setPreferredSize(new Dimension(270, 70));
		operationsTableScrollPane.setAlignmentX(LEFT_ALIGNMENT);

		JScrollPane operationRolesTableScrollPane = new JScrollPane(
				operationRolesTable);
		operationRolesTableScrollPane.setAutoscrolls(true);
		operationRolesTableScrollPane.setPreferredSize(new Dimension(270, 70));
		operationRolesTableScrollPane.setAlignmentX(LEFT_ALIGNMENT);
		
		JSplitPane operationRolesSplitPane = new JSplitPane(
				JSplitPane.HORIZONTAL_SPLIT, operationsTableScrollPane,
				operationRolesTableScrollPane);
		
        JPanel operationsPanel = new JPanel(new BorderLayout());
        operationsPanel.setBorder(BorderFactory.createTitledBorder("Operations"));
        operationsPanel.add(operationRolesSplitPane, BorderLayout.CENTER);
        
        return operationsPanel;
	}
	
	protected JPanel createButtonsPanel() {
		JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));
        buttonsPanel.add(analyzeButton);
        buttonsPanel.add(createRbacModelButton);
        buttonsPanel.add(exportRbacModelButton);
        buttonsPanel.add(validateButton);
        buttonsPanel.add(resetButton);
        
        return buttonsPanel;
	}

	protected void initButtons() {
		analyzeButton = new JButton("Analyze");
		analyzeButton.setToolTipText("Analyze event log");
		analyzeButton.setActionCommand(ActionCommand.ANALYZE_LOG);
		analyzeButton.addActionListener(this);
		
		createRbacModelButton = new JButton("Create RBAC model");
        createRbacModelButton.setToolTipText("Create RBAC model");
        createRbacModelButton.setActionCommand(ActionCommand.CREATE_RBAC);
        createRbacModelButton.addActionListener(this);
        
        validateButton = new JButton("Validate");
		validateButton.setToolTipText("Validate RBAC model");
        validateButton.setActionCommand(ActionCommand.VALIDATE);
        validateButton.addActionListener(new BaseRbacModelImportListener());
        
        exportRbacModelButton = new JButton("Export");
        exportRbacModelButton.setToolTipText("Export");
        exportRbacModelButton.setActionCommand(ActionCommand.EXPORT_RBAC);
        exportRbacModelButton.addActionListener(new RbacModelExportListener());
        
        resetButton = new JButton("Reset");
        resetButton.setToolTipText("Reset");
        resetButton.setActionCommand(ActionCommand.RESET);
        resetButton.addActionListener(this);
        
        mergeExportButton = new JButton("Resolve and export");
        mergeExportButton.setToolTipText("Resolve violations of RBAC models and export");
        mergeExportButton.setActionCommand(ActionCommand.MERGE_EXPORT);
        mergeExportButton.addActionListener(new RbacModelResolvedExportListener());
        mergeExportButton.setEnabled(true);
	}
	
	protected void initTables() {
		RolesTableModel rolesTableModel = new RolesTableModel(viewModel.getProcessModel());
		AttributesTableModel attributesTableModel = new AttributesTableModel(viewModel.getProcessModel());
		OperationsTableModel tasksTableModel = new OperationsTableModel(viewModel.getProcessModel());
		AttributeOperationsTableModel attributeOperationsTableModel = new AttributeOperationsTableModel();
		OperationRolesTableModel operationRolesTableModel = new OperationRolesTableModel(viewModel.getProcessModel());
		
		rolesTable = new RolesTable(rolesTableModel);
		attributesTable = new AttributesTable(attributesTableModel);
		attributesTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting() &&
					attributesTable.getSelectedRow() != -1) {
					BPAttribute attribute = viewModel.getProcessModel().getAttributes().get(attributesTable.getSelectedRow());
					((AttributeOperationsTableModel) attributesOperationsTable.getModel()).updateTable(attribute);
				}
			}
		});
		permissionViolationsTable = new JTable(new PermissionViolationTableModel());
		
		tasksTable = new OperationsTable(tasksTableModel);
		tasksTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting() &&
					tasksTable.getSelectedRow() != -1) {
					BPActivity activity = viewModel.getProcessModel().getActivities().get(tasksTable.getSelectedRow());
					((OperationRolesTableModel) operationRolesTable.getModel()).updateTable(activity);
				}
			}
		});
		attributesOperationsTable = new AttributeOperationsTable(attributeOperationsTableModel);
		operationRolesTable = new OperationRolesTable(operationRolesTableModel);
	}
	
	protected void updateSubjectsList() {
		subjects = Arrays.copyOf(viewModel.getProcessModel()
			.getOriginatorNames().toArray(), viewModel.getProcessModel()
			.getOriginatorNames().toArray().length, String[].class);
		subjectList.setListData(subjects);		
	}
	
	protected JMenuBar createMenuBar() {
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(createFileMenu());
		
		return menuBar;
	}
	
	protected JMenu createFileMenu() {
		menu = new JMenu();
		menu.setText("File");
		
		JMenuItem menuItem;
		menuItem = new JMenuItem("Open event log...");
		menuItem.addActionListener(new EventLogImportListener());
		menuItem.setActionCommand(ActionCommand.OPEN_FILE);
		menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl O"));
		menu.add(menuItem, MENU_OPEN_LOG);
		
		menuItem = new JMenuItem("Import");
		menuItem.addActionListener(new RbacModelImportListener());
		menuItem.setActionCommand(ActionCommand.IMPORT_XML);
		//menuItem.setEnabled(false);
		menu.add(menuItem, MENU_IMPORT_RBAC_MODEL);
		
		menuItem = new JMenuItem("Export");
		menuItem.addActionListener(new RbacModelExportListener());
		menuItem.setActionCommand(ActionCommand.EXPORT_XML);
		menuItem.setEnabled(false);
		menu.add(menuItem, MENU_EXPORT_RBAC_MODEL);
		
		menuItem = new JMenuItem("Export LTL formulas");
		menuItem.addActionListener(new LTLFormulasExportListener());
		menuItem.setActionCommand(ActionCommand.EXPORT_LTL);
		menuItem.setEnabled(false);
		menu.add(menuItem, MENU_EXPORT_LTL_RULES);
		
		menuItem = new JMenuItem("Exit");
		menuItem.addActionListener(this);
		menuItem.setActionCommand(ActionCommand.EXIT);
		menu.add(menuItem);
		
		return menu;
	}
	
	protected void initRbacModelViolationsDialog() {
		JScrollPane tableScroll = new JScrollPane(permissionViolationsTable);
        tableScroll.setAutoscrolls(true);
        tableScroll.setPreferredSize(new Dimension(800, 600));
        tableScroll.setAlignmentX(LEFT_ALIGNMENT);
		
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(mergeExportButton);
        panel.add(tableScroll);
        
        validationDialog.setLocationByPlatform(true);
        validationDialog.setAutoRequestFocus(true);
        validationDialog.setTitle("Validation results");
        validationDialog.setModalityType(Dialog.ModalityType.DOCUMENT_MODAL);
        validationDialog.add(panel);
	}
	
	protected void showRbacModelValidationDialog() {
		if (validationDialog.isVisible()) {
			return;
		}
		
		RbacModelValidator validator = viewModel.getValidator();
		if (validator.isValid()) {
			JOptionPane.showMessageDialog(this,
			    "Is already valid :)",
			    "Message",
			    JOptionPane.INFORMATION_MESSAGE);
		} else {
			((PermissionViolationTableModel) permissionViolationsTable.getModel())
			.setPermissionViolations(validator.getViolations());
			validationDialog.pack();
	        validationDialog.setVisible(true);
		}
	}
	
	protected void updateVisualizations() {
		StateEnum state = viewModel.getApplication().getStateManager().getState();
		
		analyzeButton.setEnabled(false);
		createRbacModelButton.setEnabled(false);
		exportRbacModelButton.setEnabled(false);
		validateButton.setEnabled(false);
		resetButton.setEnabled(true);
		
		menu.getItem(MENU_OPEN_LOG).setEnabled(true);
		menu.getItem(MENU_IMPORT_RBAC_MODEL).setEnabled(false);
		menu.getItem(MENU_EXPORT_LTL_RULES).setEnabled(false);
		menu.getItem(MENU_EXPORT_RBAC_MODEL).setEnabled(false);
		
		ProcessModel processModel = viewModel.getProcessModel();
		
		switch (state) {
			case STATE_LOG_IMPORTED:
				analyzeButton.setEnabled(true);
				menu.getItem(MENU_OPEN_LOG).setEnabled(false);
				break;
			case STATE_LOG_ANALYZED:
				createRbacModelButton.setEnabled(true);
				messageWriter.write("Log analyzed");
				messageWriter.write("Cases: " + processModel.getNumberOfCases());
				messageWriter.write("Events: " + processModel.getNumberOfEvents());
				messageWriter.write("Subjects: " + processModel.getOriginators().size());
				messageWriter.write("Roles: " + processModel.getRoles().size());
				messageWriter.write("Activities: " + processModel.getActivities().size());
				messageWriter.write("Data attributes: " + processModel.getAttributes().size());
				updateSubjectsList();
				updateTables();
				break;
			case STATE_RBAC_MODEL_CREATED:
				exportRbacModelButton.setEnabled(true);
				validateButton.setEnabled(true);
				menu.getItem(MENU_EXPORT_LTL_RULES).setEnabled(true);
				menu.getItem(MENU_EXPORT_RBAC_MODEL).setEnabled(true);
				messageWriter.write("RBAC model created for export and validation.");
				messageWriter.write("Subjects: " + processModel.getOriginators().size());
				messageWriter.write("Roles: " + processModel.getRoles().size());
				messageWriter.write("Activities: " + processModel.getActivities().size());
				messageWriter.write("Data attributes: " + processModel.getAttributes().size());
				updateSubjectsList();
				updateTables();
				break;
			case STATE_BASE_RBAC_MODEL_IMPORTED:
				exportRbacModelButton.setEnabled(true);
				validateButton.setEnabled(true);
			case STATE_START:
				resetButton.setEnabled(false);
				menu.getItem(MENU_IMPORT_RBAC_MODEL).setEnabled(true);
				this.subjects = new String[]{};
				this.subjectList.setListData(this.subjects);
				updateSubjectsList();
				updateTables();
			default:
				break;
		}
	}
	
	protected void updateTables() {
		((RolesTableModel) rolesTable.getModel()).updateTable();
		((AttributesTableModel) attributesTable.getModel()).updateTable();
		((OperationsTableModel) tasksTable.getModel()).updateTable();
		((AttributeOperationsTableModel) attributesOperationsTable.getModel()).updateTable();
		((OperationRolesTableModel) operationRolesTable.getModel()).updateTable();
		rolesTable.repaint();
		attributesTable.repaint();
		tasksTable.repaint();
		attributesOperationsTable.repaint();
		operationRolesTable.repaint();
	}
	
	protected void reset() {
		viewModel.reset();
		updateVisualizations();
	}
	
	/**
	 * 
	 * Action listeners
	 * 
	 */
	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();
		if (action.equals(ActionCommand.EXIT)) {
			System.exit(0);
		}
		
		if (action.equals(ActionCommand.ANALYZE_LOG)) {
			if (xesImport.getCommonFile() != null) {
				viewModel.createProcessModelFromImport(xesImport);
				updateVisualizations();
			}
		}
		
		if (action.equals(ActionCommand.CREATE_RBAC)) {
			if (viewModel.getProcessModel().getRoles().size() > 0) {
				viewModel.createRbacModel();
				updateVisualizations();
			} else {
				JOptionPane.showMessageDialog(this,
				    "Cannot create RBAC model, no role information provided.",
				    "Error",
				    JOptionPane.ERROR_MESSAGE);
			}
		}
		
		if (action.equals(ActionCommand.ADD_ROLE)) {
			List<Object> row = new ArrayList<Object>();
			row.add(new Boolean(false));
			row.add("");
			
			((RolesTableModel) rolesTable.getModel()).addRow(row);
			((RolesTableModel) rolesTable.getModel()).updateTable();
		}
		
		if (action.equals(ActionCommand.REMOVE_ROLE)) {
			int row = rolesTable.getSelectedRow();
			if (row != -1) {
				((RolesTableModel) rolesTable.getModel()).removeRow(row);
			}
		}
		
		if (action.equals(ActionCommand.RESET)) {
			reset();
		}
	}
	
	private class EventLogImportListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			fileChooser = new JFileChooser();
			fileChooser.setDialogTitle("Open event log");
			fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			fileChooser.setSelectedFile(fileChooser.getCurrentDirectory());
			fileChooser.setMultiSelectionEnabled(false);
//			fileChooser.setAcceptAllFileFilterUsed(true);
//			fileChooser.addChoosableFileFilter(xesImport.getFileFilter());
//			fileChooser.addChoosableFileFilter(rbacModelImport.getFileFilter());
			
			int retVal = fileChooser.showOpenDialog(MainFrame.this);
			if (retVal == JFileChooser.APPROVE_OPTION) {
				reset();
				xesImport.importFile(fileChooser.getSelectedFile());
				viewModel.getApplication().getStateManager().updateState(StateEnum.STATE_LOG_IMPORTED);
				
				messageWriter.write("Event log imported: " + xesImport.getCommonFile().getFile().getPath());
				updateVisualizations();
			}
		}
	}
	
	private class RbacModelImportListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			fileChooser = new JFileChooser();
			fileChooser.setDialogTitle("Import RBAC model");
			fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			fileChooser.setSelectedFile(fileChooser.getCurrentDirectory());
			fileChooser.setMultiSelectionEnabled(false);
			fileChooser.setAcceptAllFileFilterUsed(false);
			fileChooser.addChoosableFileFilter(rbacModelImport.getFileFilter());
			int retVal = fileChooser.showOpenDialog(MainFrame.this);
			if (retVal == JFileChooser.APPROVE_OPTION) {
				reset();
				rbacModelImport.importFile(fileChooser.getSelectedFile());
				viewModel.createRbacModelFromImport(rbacModelImport);
				
				messageWriter.write("RBAC model imported: " + rbacModelImport.getCommonFile().getFile().getName());
				updateVisualizations();
			}
		}
	}
	
	private class BaseRbacModelImportListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			fileChooser = new JFileChooser();
			fileChooser.setDialogTitle("Import base RBAC model");
			fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			fileChooser.setSelectedFile(fileChooser.getCurrentDirectory());
			fileChooser.setMultiSelectionEnabled(false);
			fileChooser.setAcceptAllFileFilterUsed(false);
			fileChooser.addChoosableFileFilter(rbacModelImport.getFileFilter());

			int retVal = fileChooser.showOpenDialog(MainFrame.this);
			if (retVal == JFileChooser.APPROVE_OPTION) {
				rbacModelImport.importFile(fileChooser.getSelectedFile());
				viewModel.createBaseRbacModelFromImport(rbacModelImport);
				
				messageWriter.write("Base RBAC model imported: " + rbacModelImport.getCommonFile().getFile().getName());
				showRbacModelValidationDialog();
			}
		}
	}
	
	private class RbacModelExportListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			fileChooser = new JFileChooser();
			fileChooser.setDialogTitle("Export RBAC model");
			fileChooser.setAcceptAllFileFilterUsed(false);
			fileChooser.addChoosableFileFilter(rbacModelImport.getFileFilter());
			
			int retVal = fileChooser.showSaveDialog(MainFrame.this);
			if (retVal == JFileChooser.APPROVE_OPTION) {
				File toImport = fileChooser.getSelectedFile();
				if (!(fileChooser.getSelectedFile().getName().endsWith(".xml"))) {
					toImport = new File(fileChooser.getSelectedFile().getAbsolutePath() + ".xml");
				}
				RbacModelFile rbacModelFile = new RbacModelFile(toImport);
				try {
					RbacModelXmlWriter.write(rbacModelFile, viewModel.getRbacModel());
				} catch (ParserConfigurationException e1) {
					e1.printStackTrace();
				} catch (TransformerFactoryConfigurationError e1) {
					e1.printStackTrace();
				} catch (TransformerException e1) {
					e1.printStackTrace();
				}
				
				messageWriter.write("RBAC model exported: " + rbacModelFile.getFile().getPath());
			}
		}
	}
	
	private class RbacModelResolvedExportListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			fileChooser = new JFileChooser();
			fileChooser.setDialogTitle("Export RBAC model");
			fileChooser.setAcceptAllFileFilterUsed(false);
			fileChooser.addChoosableFileFilter(rbacModelImport.getFileFilter());
			
			int retVal = fileChooser.showSaveDialog(MainFrame.this);
			if (retVal == JFileChooser.APPROVE_OPTION) {
				File toImport = fileChooser.getSelectedFile();
				if (!(fileChooser.getSelectedFile().getName().endsWith(".xml"))) {
					toImport = new File(fileChooser.getSelectedFile().getAbsolutePath() + ".xml");
				}
				RbacModelFile rbacModelFile = new RbacModelFile(toImport);
				viewModel.getValidator().resolve();
				try {
					RbacModelXmlWriter.write(rbacModelFile, viewModel.getValidator().getModel());
				} catch (ParserConfigurationException e1) {
					e1.printStackTrace();
				} catch (TransformerFactoryConfigurationError e1) {
					e1.printStackTrace();
				} catch (TransformerException e1) {
					e1.printStackTrace();
				}
				viewModel.getValidator().reset();
				validationDialog.dispose();
				messageWriter.write("Validated RBAC model exported: " + rbacModelFile.getFile().getPath());
			}
		}
	}
	
	private class LTLFormulasExportListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			fileChooser = new JFileChooser();
			fileChooser.setDialogTitle("Export LTL file");
			fileChooser.setAcceptAllFileFilterUsed(false);
			fileChooser.addChoosableFileFilter(LTLFile.getFileFilter());
			
			int retVal = fileChooser.showSaveDialog(MainFrame.this);
			if (retVal == JFileChooser.APPROVE_OPTION) {
				File toImport = fileChooser.getSelectedFile();
				if (!(fileChooser.getSelectedFile().getName().endsWith(".ltl"))) {
					toImport = new File(fileChooser.getSelectedFile().getAbsolutePath() + ".ltl");
				}
				LTLFile ltlFile = new LTLFile(toImport);
				LTLWriter ltlWriter = new LTLWriter();
				ltlWriter.write(ltlFile, viewModel.getRbacModel());
				messageWriter.write("LTL file exported: " + ltlFile.getFile().getPath());
			}
		}
	}
	
	private class MessageWriter {

		protected JTextPane textPane;
		
		public MessageWriter(JTextPane textPane) {
			this.textPane = textPane;
		}
		
		public void write(String message) {
			try {
				Document doc = textPane.getDocument();
				doc.insertString(doc.getLength(), "> " + message + "\n", null);
			} catch (BadLocationException exc) {
				exc.printStackTrace();
			}
		}
	}
}


