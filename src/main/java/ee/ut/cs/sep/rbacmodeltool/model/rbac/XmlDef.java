package ee.ut.cs.sep.rbacmodeltool.model.rbac;

public class XmlDef {

	public static final String OPERATIONS 	= "operations";
	public static final String ROLES 		= "roles";
	public static final String ROLE 		= "role";
	public static final String SUBJECTS 	= "subjects";
	public static final String SUBJECT 		= "subject";
	public static final String OPERATION 	= "operation";
	public static final String RESOURCES 	= "resources";
	public static final String RESOURCE 	= "resource";
	public static final String PERMISSIONS 	= "permissions";
	public static final String PERMISSION 	= "permission";
	public static final String VALUES 		= "values";
	public static final String VALUE 		= "value";
	public static final String ACTION 		= "action";
	public static final String OPERATION_ID = "id";
	public static final String ROLE_ID 		= "id";
	public static final String RESOURCE_ID 	= "id";
	public static final String SUBJECT_ID	= "id";
	public static final String REFID 		= "refid";
	public static final String RBAC 		= "rbac";
	
}
