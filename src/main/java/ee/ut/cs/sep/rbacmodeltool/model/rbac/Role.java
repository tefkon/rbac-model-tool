package ee.ut.cs.sep.rbacmodeltool.model.rbac;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Role extends RbacElement {
	
	public HashMap<String, Permission> permissions = new HashMap<String, Permission>();
	
	public Role(String name, RbacModel model) {
		super(name, model);
	}

	public Set<Subject> subjects = new HashSet<Subject>();
    
	public Role addSubject(Subject subject) {
		this.subjects.add(subject);
		
		return this;
	}
	
	public List<Subject> getSubjects() {
		ArrayList<Subject> result = new ArrayList<Subject>();
		for (Subject subject : this.subjects) {
			result.add(subject);
		}
		
		return result;
	}
	
	public List<Permission> getPermissions() {
		List<Permission> result = new ArrayList<Permission>();
		for (Permission permission : permissions.values()) {
			result.add(permission);
		}
		
		return result;
	}

	public boolean hasPermission(Permission permission) {
		if (permission == null) {
			
			return false;
		}
		
		return this.hasPermission(permission.getName());
	}
	
	public boolean hasPermission(String identificator) {
		
		return permissions.containsKey(identificator);
	}
	
	public Role addPermission(Permission permission) {
		this.permissions.put(permission.getName(), permission);
		
		return this;
	}
	
	public Role removePermission(Permission permission) {
		
		return this.removePermission(permission.getName());
	}
	
	public Role removePermission(String identificator) {
		this.permissions.remove(identificator);
		
		return this;
	}

}
