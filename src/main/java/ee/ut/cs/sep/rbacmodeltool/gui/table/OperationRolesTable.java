package ee.ut.cs.sep.rbacmodeltool.gui.table;

import java.awt.Dimension;
import javax.swing.JTable;
import ee.ut.cs.sep.rbacmodeltool.gui.tablemodel.OperationRolesTableModel;

@SuppressWarnings("serial")
public class OperationRolesTable extends JTable {

	public OperationRolesTable(OperationRolesTableModel operationRolesTableModel) {
		this.setPreferredScrollableViewportSize(new Dimension(180, 70));
		this.setFillsViewportHeight(true);
		this.setModel(operationRolesTableModel);
	}
	
}
