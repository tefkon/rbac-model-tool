package ee.ut.cs.sep.rbacmodeltool.gui.table;

import java.awt.Dimension;

import javax.swing.JTable;

import ee.ut.cs.sep.rbacmodeltool.gui.tablemodel.AttributeOperationsTableModel;

@SuppressWarnings("serial")
public class AttributeOperationsTable extends JTable {
		
	public AttributeOperationsTable(AttributeOperationsTableModel attributeOperationsTableModel) {
		this.setPreferredScrollableViewportSize(new Dimension(180, 70));
		this.setFillsViewportHeight(true);
		this.setModel(attributeOperationsTableModel);
	}
	
}
