package ee.ut.cs.sep.rbacmodeltool.model.rbac.algorithms;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import ee.ut.cs.sep.rbacmodeltool.model.rbac.Operation;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.Permission;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.RbacModel;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.ResourceAttribute;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.Role;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.Subject;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.XmlDef;

public class RbacModelXmlHandler extends DefaultHandler {

	protected RbacModel rbacModel;
	
	// "elementId" -> "name"
	protected HashMap<String, String> resourceIdMapping = new HashMap<String, String>();
	protected HashMap<String, String> subjectIdMapping = new HashMap<String, String>();
	protected HashMap<String, String> roleIdMapping = new HashMap<String, String>();
	protected HashMap<String, String> operationIdMapping = new HashMap<String, String>();
	protected HashMap<String, Set<String>> resourceIdValues = new HashMap<String, Set<String>>();
	
	private boolean isOperations = false;
	private boolean isRoles = false;
	private boolean isResources = false;
	private boolean isPermissions = false;
	private boolean isResource = false;
	private boolean isValues = false;
	private boolean isValue = false;
	private boolean isSubjects = false;
	private boolean isSubject = false;
	
	private String currentOperationId = "";
	private String currentRoleId = "";
	private String currentResourceId = "";
	private String currentSubjectId = "";
	
	public RbacModelXmlHandler(RbacModel rbacModel) {
		this.rbacModel = rbacModel;
	}
	
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		
		if (qName.equalsIgnoreCase(XmlDef.OPERATIONS)) {
			isOperations = true;
		}
		
		if (qName.equalsIgnoreCase(XmlDef.OPERATION)) {
			if (isOperations && !isResource) {
				String activityName = "";
				for (int i = 0; i < attributes.getLength(); i++) {
					String key = attributes.getQName(i);
					String keyValue = attributes.getValue(i);
					if (key.equals(XmlDef.OPERATION_ID)) {
						currentOperationId = keyValue;
					}
					
					if (key.equals("name")) {
						activityName = keyValue;
					}
				}
				
				if (!(currentOperationId.isEmpty() || activityName.isEmpty())) {
					addOperation(activityName);
				}
			}
			
			if (isOperations && isResource) {
				for (int i = 0; i < attributes.getLength(); i++) {
					String key = attributes.getQName(i);
					String keyValue = attributes.getValue(i);
					if (key.equals(XmlDef.REFID)) {
						currentOperationId = keyValue;
					}
				}
			}
		}
		
		if (qName.equalsIgnoreCase(XmlDef.ROLES)) {
			isRoles = true;
		}
		
		if (qName.equalsIgnoreCase(XmlDef.ROLE)) {
			if (isRoles) {
				String roleName = "";
				for (int i = 0; i < attributes.getLength(); i++) {
					String key = attributes.getQName(i);
					String keyValue = attributes.getValue(i);
					if (key.equals(XmlDef.ROLE_ID)) {
						currentRoleId = keyValue;
					}
					
					if (key.equals("name")) {
						roleName = keyValue;
					}
				}
				
				if (!(currentRoleId.isEmpty() || roleName.isEmpty())) {
					addRole(roleName);
				}
			}
		}
		
		if (qName.equalsIgnoreCase(XmlDef.RESOURCES)) {
			isResources = true;
		}
		
		if (qName.equalsIgnoreCase(XmlDef.RESOURCE)) {
			isResource = true;
			if (isResources) {
				String resourceName = "";
				for (int i = 0; i < attributes.getLength(); i++) {
					String key = attributes.getQName(i);
					String keyValue = attributes.getValue(i);
					if (key.equals(XmlDef.RESOURCE_ID)) {
						currentResourceId = keyValue;
					}
					
					if (key.equalsIgnoreCase("name")) {
						resourceName = keyValue;
					}
				}
				
				if (!(currentResourceId.isEmpty() || resourceName.isEmpty())) {
					addResource(resourceName);
				}
			}
			
			if(isPermissions) {
				for (int i = 0; i < attributes.getLength(); i++) {
					String key = attributes.getQName(i);
					String keyValue = attributes.getValue(i);
					if (key.equals(XmlDef.REFID)) {
						currentResourceId = keyValue;
					}
				}
			}
		}
		
		if (qName.equalsIgnoreCase(XmlDef.PERMISSIONS)) {
			isPermissions = true;
		}
		
		if (qName.equalsIgnoreCase(XmlDef.PERMISSION)) {
//			System.out.println("Is permission");
			if (isPermissions) {
//				System.out.println("Adding new permission");
				String resourceName = resourceIdMapping.get(currentResourceId);
				ResourceAttribute resource = rbacModel.getResourceAttribute(resourceName);
				String roleId = "";
				String operationId = "";
				String action = "";
				
				for (int i = 0; i < attributes.getLength(); i++) {
					String key = attributes.getQName(i);
					String keyValue = attributes.getValue(i);
					if (key.equals(XmlDef.OPERATION)) {
						operationId = keyValue;
					}
					
					if (key.equals(XmlDef.ROLE)) {
						roleId = keyValue;
					}
					
					if (key.equals(XmlDef.ACTION)) {
						action = keyValue;
					}
				}
				
				if (!(roleId.isEmpty() || operationId.isEmpty())) {
					String roleName = roleIdMapping.get(roleId);
					String operationName = operationIdMapping.get(operationId);
					
//					if (operationName == null) {
//						System.out.println("Operation name is null");
//					}
					
					Role role = rbacModel.getRole(roleName);
					Operation operation = rbacModel.getOperation(operationName);
					
					Permission permission = rbacModel.createOrGetPermission(resource, operation);
					role.addPermission(permission);
				} 
			}
		}
		
		if (qName.equalsIgnoreCase(XmlDef.VALUES)) {
			if (isResources && isResource) {
				isValues = true;
			}
		}
	
		
		if (qName.equalsIgnoreCase(XmlDef.VALUE)) {
			if (isValues) {
				isValue = true;
			}
		}
		
		
		if (qName.equalsIgnoreCase(XmlDef.SUBJECTS)) {
			isSubjects = true;
		}
		
		if (qName.equalsIgnoreCase(XmlDef.SUBJECT)) {
			if (isSubjects && !isRoles) {
				String subjectName = "";
				for (int i = 0; i < attributes.getLength(); i++) {
					String key = attributes.getQName(i);
					String keyValue = attributes.getValue(i);
					if (key.equals(XmlDef.SUBJECT_ID)) {
						currentSubjectId = keyValue;
					}
					
					if (key.equals("name")) {
						subjectName = keyValue;
					}
				}
				
				if (!(currentSubjectId.isEmpty() || subjectName.isEmpty())) {
					addSubject(subjectName);
				}
				
				isSubject = true;
			}
			
			if(isSubjects && isRoles) {
				for (int i = 0; i < attributes.getLength(); i++) {
					String key = attributes.getQName(i);
					String keyValue = attributes.getValue(i);
					if (key.equals(XmlDef.REFID)) {
						currentSubjectId = keyValue;
					}
				}
			}
		}
	}

	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String contents = new String(ch, start, length);
		if (isValues && isValue && !currentResourceId.isEmpty()) {
			if (resourceIdValues.containsKey(currentResourceId)) {
				resourceIdValues.get(currentResourceId).add(contents);
			} else {
				Set<String> values = new HashSet<String>();
				values.add(contents);
				resourceIdValues.put(currentResourceId, values);
			}
		}
	}

	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		
		super.endElement(uri, localName, qName);
		
		if (qName.equalsIgnoreCase(XmlDef.OPERATIONS)) {
			isOperations = false;
		}
		
		if (qName.equalsIgnoreCase(XmlDef.RESOURCES)) {
			isResources = false;
		}
		
		if (qName.equalsIgnoreCase(XmlDef.RESOURCE)) {
			isResource = false;
//			if (isPermissions) {
//				currentResourceId = "";
//			}
			currentResourceId = "";
		}

		if (qName.equalsIgnoreCase(XmlDef.ROLES)) {
			isRoles = false;
		}
		
		if (qName.equalsIgnoreCase(XmlDef.PERMISSIONS)) {
			isPermissions = false;
			// Add values to created resources
			// Needs to be done after all the resources have created
			for(Entry<String, Set<String>> entry : resourceIdValues.entrySet()) {
				String resourceName = resourceIdMapping.get(entry.getKey());
				ResourceAttribute resource = rbacModel.getResourceAttribute(resourceName);
				if (resource != null) {
					for (String value : entry.getValue()) {
						rbacModel.addValueToResourceAttribute(value, resource);
					}
				}
			}
		}
		
		if (qName.equalsIgnoreCase(XmlDef.VALUES)) {
			isValues = false;
		}
		
		if (qName.equalsIgnoreCase(XmlDef.VALUE)) {
			isValue = false;
		}

		if (qName.equalsIgnoreCase(XmlDef.SUBJECTS)) {
			isSubjects = false;
		}
		
		if (qName.equalsIgnoreCase(XmlDef.SUBJECT)) {
			if (isRoles &&
				!currentRoleId.isEmpty() &&
				!currentSubjectId.isEmpty()) {
				
				String subjectName = subjectIdMapping.get(currentSubjectId);
				String roleName = roleIdMapping.get(currentRoleId);
				addSubjectToRole(roleName, subjectName);
			}
			
			currentSubjectId = "";
			isSubject = false;
		}
		
		if (qName.equalsIgnoreCase(XmlDef.OPERATION)) {
			
			
			if (isResource &&
				!currentOperationId.isEmpty()) {
				String operationName = operationIdMapping.get(currentOperationId);
				String resourceName = resourceIdMapping.get(currentResourceId);
				addOperationToResource(resourceName, operationName);
			}
			
			currentOperationId = "";
		}
		
		if (qName.equalsIgnoreCase(XmlDef.ROLE)) {
			currentRoleId = "";
		}
	}
	
	protected void addOperation(String operationName) {
		operationIdMapping.put(currentOperationId, operationName);
		Operation operation = rbacModel.createOrGetOperation(operationName);
		rbacModel.addOperation(operation);
	}
	
	protected void addSubject(String subjectName) {
		subjectIdMapping.put(currentSubjectId, subjectName);
		Subject subject = rbacModel.createOrGetSubject(subjectName);
		rbacModel.addSubject(subject);
	}
	
	protected void addRole(String roleName) {
		roleIdMapping.put(currentRoleId, roleName);
		Role role = rbacModel.createOrGetRole(roleName);
		rbacModel.addRole(role);
	}
	
	protected void addResource(String resourceName) {
		resourceIdMapping.put(currentResourceId, resourceName);
		ResourceAttribute resource = rbacModel.createOrGetResourceAttribute(resourceName);
		rbacModel.addResourceAttribute(resource);
	}
	
	protected void addSubjectToRole(String roleName, String subjectName) {
		Subject subject = rbacModel.getSubject(subjectName);
		Role role = rbacModel.getRole(roleName);
		role.addSubject(subject);
	}
	
	protected void addOperationToResource(String resourceName, String operationName) {
		ResourceAttribute resource = rbacModel.getResourceAttribute(resourceName);
		Operation operation = rbacModel.getOperation(operationName);
		resource.addOperation(operation);
	}

}
