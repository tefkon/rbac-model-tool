package ee.ut.cs.sep.rbacmodeltool.gui.tablemodel;

import javax.swing.table.AbstractTableModel;

import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.ProcessModel;

@SuppressWarnings("serial")
public class OperationsTableModel extends AbstractTableModel {

	protected String[] columnNames = { "Operation" };
	
	protected ProcessModel processModel;
	
	public OperationsTableModel(ProcessModel processModel) {
		this.processModel = processModel;
	}
	
	public int getColumnCount() {
		
		return columnNames.length;
	}

	public int getRowCount() {
		
		return processModel.getActivities().size();
	}

	public Object getValueAt(int row, int col) {
		
		return processModel.getActivities().get(row).getName();
	}

	public void setValueAt(Object value, int row, int col) {
		processModel.getActivities().get(row).setName((String) value);
		fireTableCellUpdated(processModel.getActivities().size(), processModel.getActivities().size());
	}
	
	public String getColumnName(int col) {
		
		return columnNames[col];
	}

	public boolean isCellEditable(int row, int col) {

		return false;
	}
	
	public Class<?> getColumnClass(int col) {
		
		return String.class;
	}
	
	public void updateTable() {
		super.fireTableDataChanged();
	}
}
