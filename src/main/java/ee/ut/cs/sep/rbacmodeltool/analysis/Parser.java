package ee.ut.cs.sep.rbacmodeltool.analysis;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.ProcessModel;
import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.algorithms.XesHandler;

public class Parser {
	
	static final String ORIGINATOR 			= "originator";
	static final String TASK 				= "workflowmodelelement";
	static final String PROCESS_INSTANCE 	= "processinstance";
	static final String DATA_ATTRIBUTE	 	= "attribute";
	
	private ProcessModel processModel;
 	
 	public Parser(ProcessModel processModel) {
 		this.processModel = processModel;
 	}
 	
 	public ProcessModel getProcessModel() {
 		
 		return this.processModel;
 	}
	
	public void parse(File file) {
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			
//			MxmlHandler handler = new MxmlHandler();
//			saxParser.parse(file, handler);
			
			XesHandler handler = new XesHandler(this.processModel);
			saxParser.parse(file, handler);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}	
