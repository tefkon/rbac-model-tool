package ee.ut.cs.sep.rbacmodeltool;

import java.io.File;

import ee.ut.cs.sep.rbacmodeltool.gui.MainFrame;
import ee.ut.cs.sep.rbacmodeltool.importing.businessprocess.XesImport;
import ee.ut.cs.sep.rbacmodeltool.importing.rbac.RbacModelImport;
import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.ProcessModel;
import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.algorithms.XesReader;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.RbacModel;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.RbacModelConverter;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.algorithms.RbacModelXmlReader;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.validator.RbacModelValidator;

/**
 * Intermediary for models and visualization
 * 
 * @author Taivo
 */
public class ViewModel {
	
	private Application application;
	private ProcessModel processModel = new ProcessModel();		// Always using one instance of process model
	private RbacModelConverter rbacModelConverter = new RbacModelConverter();
	private MainFrame view = null;
	private RbacModel rbacModel = null;
	private RbacModel baseRbacModel = null;
	private RbacModelValidator validator = null;
	
	public ViewModel(Application application) {
		this.application = application;
	}
	
	public Application getApplication() {
		
		return this.application;
	}
	
	public MainFrame getView() {
		return view;
	}

	public void setView(MainFrame view) {
		this.view = view;
	}

	public ProcessModel getProcessModel() {
		return this.processModel;
	}

	public void setProcessModel(ProcessModel processModel) {
		this.processModel = processModel;
	}
	
	public ProcessModel createProcessModelFromImport(XesImport xesImport) {
		processModel = XesReader.read(xesImport.getCommonFile(), processModel);
		setProcessModel(processModel);
		getApplication().getStateManager().updateState(StateEnum.STATE_LOG_ANALYZED);
		
		return processModel;
	}

	public RbacModel createRbacModel() {
		rbacModel = rbacModelConverter.createRbacModelFromProcessModel(processModel);
		getApplication().getStateManager().updateState(StateEnum.STATE_RBAC_MODEL_CREATED);
		setRbacModel(rbacModel);
		
		return rbacModel;
	}
	
	public RbacModel createRbacModelFromImport(RbacModelImport rbacModelImport) {
		RbacModel rbacModel = RbacModelXmlReader.read(rbacModelImport.getCommonFile());
		getApplication().getStateManager().updateState(StateEnum.STATE_LOG_ANALYZED);
		processModel = rbacModelConverter.createProcessModelFromRbacModel(processModel, rbacModel);
		setRbacModel(rbacModel);
		
		return rbacModel;
	}
	
	public RbacModel createBaseRbacModelFromImport(RbacModelImport rbacModelImport) {
		RbacModel rbacModel = RbacModelXmlReader.read(rbacModelImport.getCommonFile());
		getApplication().getStateManager().updateState(StateEnum.STATE_BASE_RBAC_MODEL_IMPORTED);
		setBaseRbacModel(rbacModel);
		
		return rbacModel;
	}
	
	public RbacModel getRbacModel() {
		return rbacModel;
	}

	public void setRbacModel(RbacModel rbacModel) {
		this.rbacModel = rbacModel;
	}

	public RbacModel getBaseRbacModel() {
		
		return baseRbacModel;
	}

	public void setBaseRbacModel(RbacModel baseRbacModel) {
		validator = new RbacModelValidator(baseRbacModel, rbacModel);
		
		this.baseRbacModel = baseRbacModel;
	}

	public RbacModelConverter getRbacModelConverter() {
		return rbacModelConverter;
	}

	public void setRbacModelConverter(RbacModelConverter rbacModelConverter) {
		this.rbacModelConverter = rbacModelConverter;
	}

	public RbacModelValidator getValidator() {
		
		return validator;
	}

	public void reset() {
		getApplication().getStateManager().updateState(StateEnum.STATE_START);
		if (rbacModel != null) {
			rbacModel.reset();
		}
		baseRbacModel = null;
		processModel.reset();
	}
	
}
