package ee.ut.cs.sep.rbacmodeltool.model.rbac.algorithms;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import ee.ut.cs.sep.rbacmodeltool.io.rbac.RbacModelFile;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.Operation;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.RbacModel;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.ResourceAttribute;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.Role;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.Subject;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.XmlDef;

public class RbacModelXmlWriter {

	private static final String XML_SCHEMA_INSTANCE = "http://www.w3.org/2001/XMLSchema-instance";
	private static final String XSD_LOCATION 		= "rbac.xsd"; // TODO : can be external location
	
	public static void write(RbacModelFile rbacModelFile, RbacModel model) throws ParserConfigurationException, TransformerFactoryConfigurationError, TransformerException {
		HashMap<String, String> subjectIdMapping 	= new HashMap<String, String>();
		HashMap<String, String> resourceIdMapping = new HashMap<String, String>();
		HashMap<String, String> roleIdMapping 	= new HashMap<String, String>();
		HashMap<String, String> operationIdMapping = new HashMap<String, String>();
		
		int subjectId 	= 1;
		int operationId = 1;
		int roleId 		= 1;
		int resourceId 	= 1;
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder loader;
		loader = factory.newDocumentBuilder();
		Document dom = loader.newDocument();
		Element rbac = dom.createElement(XmlDef.RBAC);
		rbac.setAttribute("xmlns:xsi", XML_SCHEMA_INSTANCE);
		// TODO : change it to external location in the future
		rbac.setAttribute("xsi:noNamespaceSchemaLocation", XSD_LOCATION);
		
		Element subjectsElement = dom.createElement(XmlDef.SUBJECTS);
		Element subjectElement;
		for(Subject subject : model.getSubjects()) {
			String id = XmlDef.SUBJECT + subjectId++;
			subjectIdMapping.put(subject.getName(), id);
			
			subjectElement = dom.createElement(XmlDef.SUBJECT);
			subjectElement.setAttribute("id", id);
			subjectElement.setAttribute("name", subject.getName());
			subjectsElement.appendChild(subjectElement);
		}
		rbac.appendChild(subjectsElement);
		
		Element operationsElement = dom.createElement(XmlDef.OPERATIONS);
		Element operationElement;
		for(Operation operation : model.getOperations()) {
			String id = XmlDef.OPERATION + operationId++;
			operationIdMapping.put(operation.getName(), id);
			
			operationElement = dom.createElement(XmlDef.OPERATION);
			operationElement.setAttribute("id", id);
			operationElement.setAttribute("name", operation.getName());
			operationsElement.appendChild(operationElement);
		}
		rbac.appendChild(operationsElement);
		
		Element rolesElement = dom.createElement(XmlDef.ROLES);
		Element roleElement;
		Element roleSubjectsElement;
		Element roleSubjectElement;
		for(Role role : model.getRoles()) {
			String id = XmlDef.ROLE + roleId++;
			roleIdMapping.put(role.getName(), id);
			roleElement = dom.createElement(XmlDef.ROLE);
			roleElement.setAttribute("id", id);
			roleElement.setAttribute("name", role.getName());
			roleSubjectsElement = dom.createElement(XmlDef.SUBJECTS);
			for (Subject subject : role.getSubjects()) {
				String roleSubjectId = subjectIdMapping.get(subject.getName());
				roleSubjectElement = dom.createElement(XmlDef.SUBJECT);
				roleSubjectElement.setAttribute(XmlDef.REFID, roleSubjectId);
				roleSubjectsElement.appendChild(roleSubjectElement);
			}
			roleElement.appendChild(roleSubjectsElement);
			rolesElement.appendChild(roleElement);
		}
		rbac.appendChild(rolesElement);
		
		Element resourcesElement = dom.createElement(XmlDef.RESOURCES);
		Element resourceElement;
		Element valuesElement;
		Element valueElement;
		Element resourceOperationsElement;
		Element resourceOperationElement;
		for (ResourceAttribute resource : model.getResourceAttributes()) {
			String id = XmlDef.RESOURCE + resourceId++;
			resourceIdMapping.put(resource.getName(), id);
			resourceElement = dom.createElement(XmlDef.RESOURCE);
			resourceElement.setAttribute("id", id);
			resourceElement.setAttribute("name", resource.getName());
			valuesElement = dom.createElement(XmlDef.VALUES);
			Set<Object> values = resource.getValues();
			for(Object value : values) {
				valueElement = dom.createElement(XmlDef.VALUE);
				valueElement.appendChild(dom.createTextNode((String) value));
				valuesElement.appendChild(valueElement);
			}
			
			resourceOperationsElement = dom.createElement(XmlDef.OPERATIONS);
			for (Operation operation : resource.getOperations()) {
				String resourceOperationId = operationIdMapping.get(operation.getName());
				resourceOperationElement = dom.createElement(XmlDef.OPERATION);
				resourceOperationElement.setAttribute(XmlDef.REFID, resourceOperationId);
				resourceOperationsElement.appendChild(resourceOperationElement);
			}
			
			resourceElement.appendChild(resourceOperationsElement);
			resourceElement.appendChild(valuesElement);
			resourcesElement.appendChild(resourceElement);
		}
		rbac.appendChild(resourcesElement);
		
		Element permissionsElement = dom.createElement(XmlDef.PERMISSIONS);
		Element permissionResourceElement;
		Element permissionElement;
		for (Entry<ResourceAttribute, HashMap<Operation, HashSet<Role>>> entry : model.getRolePermissionAssignments().entrySet()) {
			ResourceAttribute resource = entry.getKey();
			String permissionResourceId = resourceIdMapping.get(resource.getName());
			permissionResourceElement = dom.createElement(XmlDef.RESOURCE);
			permissionResourceElement.setAttribute(XmlDef.REFID, permissionResourceId);
			for (Entry<Operation, HashSet<Role>> subEntry : entry.getValue().entrySet()) {
				HashSet<Role> roles = subEntry.getValue();
				Operation operation = subEntry.getKey();
				String activityId = operationIdMapping.get(operation.getName());
				String action = "";
				for (Role role : roles) {
					String permissionRroleId = roleIdMapping.get(role.getName());
					permissionElement = dom.createElement(XmlDef.PERMISSION);
					permissionElement.setAttribute(XmlDef.OPERATION, activityId);
					permissionElement.setAttribute(XmlDef.ACTION, action);
					permissionElement.setAttribute(XmlDef.ROLE, permissionRroleId);
					permissionResourceElement.appendChild(permissionElement);
//					
//					if (role.getName().equals("A2_4")) {
//						System.out.println("X: " + role.getName() + " "  + operation.getName() + " " + resource.getName());
//					}
					
				}
			}
			
			permissionsElement.appendChild(permissionResourceElement);
		}
		
		rbac.appendChild(permissionsElement);
		
		dom.appendChild(rbac);
		
		Source source = new DOMSource(dom);
		Result result = new StreamResult(rbacModelFile.getFile());
		
		Transformer tf = TransformerFactory.newInstance().newTransformer();

        tf.setOutputProperty(OutputKeys.INDENT, "yes");
        tf.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        tf.transform(source, result); 
	}
	
}
