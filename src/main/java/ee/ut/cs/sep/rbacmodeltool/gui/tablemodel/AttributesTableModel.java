package ee.ut.cs.sep.rbacmodeltool.gui.tablemodel;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.BPAttribute;
import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.ProcessModel;

@SuppressWarnings("serial")
public class AttributesTableModel extends AbstractTableModel {

	public static final String COLUMN_HAS_ATTRIBUTE = "Include";
	public static final String COLUMN_ATTRIBUTE = "Attribute";
	public static final String COLUMN_VALUES = "Values";

	private List<String> columnNames = new ArrayList<String>();
	
	private List<Boolean> selected = new ArrayList<Boolean>();
	private ProcessModel processModel;
	
	public AttributesTableModel(ProcessModel processModel) {
		this.processModel = processModel;
		
		columnNames.add(COLUMN_HAS_ATTRIBUTE);
		columnNames.add(COLUMN_ATTRIBUTE);
		columnNames.add(COLUMN_VALUES);
	}
	
	public int getColumnCount() {
		
		return columnNames.size();
	}

	public int getRowCount() {
		
		return processModel.getAttributes().size();
	}

	public Object getValueAt(int row, int col) {
		if (col == 0) {
			return (Boolean) this.processModel.getAttributes().get(row).isSelected();
		}
		
		if (col == 1) {
			return this.processModel.getAttributes().get(row).getName();
		}
		
		return this.processModel.getAttributes().get(row).getValues().toString();
	}

	public void setValueAt(Object value, int row, int col) {
		if (col == 0) {
			this.processModel.getAttributes().get(row).setSelected((Boolean) value);
		} else {
			this.processModel.getAttributes().get(row).setName((String) value);
		}
		
		fireTableCellUpdated(row, col);
	}
	
	public String getColumnName(int col) {
		
		return columnNames.get(col);
	}

	public boolean isCellEditable(int row, int col) {

		return col == 0;
	}
	
	public Class<?> getColumnClass(int col) {
		
		return (col == 0) ? Boolean.class : String.class;
	}
	
	public void updateTable() {
		for (int i = 0; i < processModel.getAttributes().size(); i++) {
			selected.add(i, new Boolean(true));
		}
		
		super.fireTableDataChanged();
	}
	
	public void updateTable(BPAttribute attribute) {
		for (int i = 0; i < processModel.getAttributes().size(); i++) {
			selected.add(i, processModel.hasAttribute(attribute));
		}
		
		super.fireTableDataChanged();
	}
	
}
