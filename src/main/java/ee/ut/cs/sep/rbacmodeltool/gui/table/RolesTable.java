package ee.ut.cs.sep.rbacmodeltool.gui.table;

import java.awt.Dimension;

import javax.swing.JTable;

import ee.ut.cs.sep.rbacmodeltool.gui.tablemodel.RolesTableModel;

@SuppressWarnings("serial")
public class RolesTable extends JTable {

	public RolesTable(RolesTableModel roleTableModel) {
		this.setPreferredScrollableViewportSize(new Dimension(390, 70));
		this.setFillsViewportHeight(true);
		this.setModel(roleTableModel);
	}
	
}
