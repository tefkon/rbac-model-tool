package ee.ut.cs.sep.rbacmodeltool.io.rbac;

import java.io.File;

import ee.ut.cs.sep.rbacmodeltool.io.CommonFile;

/**
 * Encapsulates RBAC model XML file
 * @author Taivo
 */
public class RbacModelFile extends CommonFile {
	
	public RbacModelFile(File file) {
		super(file);
	}

	public String getFileExtension() {
		
		return "xml";
	}
}
