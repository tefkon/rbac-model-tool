package ee.ut.cs.sep.rbacmodeltool.io.businessprocess;

import java.io.File;
import ee.ut.cs.sep.rbacmodeltool.io.CommonFile;

public class LogFile extends CommonFile {

	public LogFile(File file) {
		super(file);
	}

	public String getFileExtension() {
		
		return "xes";
	}
	
}
