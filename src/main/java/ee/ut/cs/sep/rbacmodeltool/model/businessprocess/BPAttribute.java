package ee.ut.cs.sep.rbacmodeltool.model.businessprocess;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Global attribute and its values used during one Process instance
 */
public class BPAttribute {
	
	private String name;
	private Set<Object> values = new HashSet<Object>();
	private HashMap<String, BPActivity> activities = new HashMap<String, BPActivity>();
	private Boolean isSelected = new Boolean(true);
	
	public BPAttribute(String name) {
		this.name = name;
	}
	
	public String getName() {
		
		return this.name;
	}
	
	public BPAttribute setName(String name) {
		this.name = name;
		
		return this;
	}
	
	public BPAttribute setValues(Set<Object> values) {
		this.values = values;
		
		return this;
	}
	
	public BPAttribute addValue(Object value) {
		values.add(value);
		
		return this;
	}
	
	public Set<Object> getValues() {
		
		return this.values;
	}
	
	public BPAttribute addActivity(BPActivity activity) {
		this.activities.put(activity.getName(), activity);
		
		return this;
	}
	
	public List<BPActivity> getActivities() {
		List<BPActivity> result = new ArrayList<BPActivity>();
		for (BPActivity activity : this.activities.values()) {
			result.add(activity);
		}
		
		return result;
	}

	public Boolean isSelected() {
		
		return isSelected;
	}

	public void setSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}
}
