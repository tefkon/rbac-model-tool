package ee.ut.cs.sep.rbacmodeltool.gui.tablemodel;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import ee.ut.cs.sep.rbacmodeltool.model.rbac.validator.PermissionViolation;

@SuppressWarnings("serial")
public class PermissionViolationTableModel extends AbstractTableModel {

	private String[] columnNames = { "Accept" , "Resource", "Role", "Operation", "Reason" };
	
	private List<PermissionViolation> permissionViolations = new ArrayList<PermissionViolation>();
	
	public void setPermissionViolations(List<PermissionViolation> permissionViolations) {
		this.permissionViolations = permissionViolations;
	}
	
	public int getRowCount() {
		
		return permissionViolations.size();
	}

	public int getColumnCount() {
		
		return columnNames.length;
	}

	public Object getValueAt(int row, int col) {
		if (col == 0) {
			return (Boolean) permissionViolations.get(row).isKeep();
		} 
		
		if (col == 1) {
			return permissionViolations.get(row).getResourceName();
		}
		
		if (col == 2) {
			return permissionViolations.get(row).getRoleName();
		}
		
		if (col == 3) {
			return permissionViolations.get(row).getOperationName();
		}
		
		if (col == 4) {
			return permissionViolations.get(row).getReason();
		}
		
		return null;
	}
	
	public void setValueAt(Object value, int row, int col) {
		if (col == 0) {
			permissionViolations.get(row).setKeep((Boolean) value);
		}
		
		
		fireTableCellUpdated(row, col);
	}

	public boolean isCellEditable(int row, int col) {
		
		return col == 0;
	}
	
	public String getColumnName(int col) {
		
		return columnNames[col];
	}
	
	public Class<?> getColumnClass(int col) {
		
		return (col == 0) ? Boolean.class : String.class;
	}
	
}
