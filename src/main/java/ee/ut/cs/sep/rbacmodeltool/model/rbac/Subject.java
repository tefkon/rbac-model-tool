package ee.ut.cs.sep.rbacmodeltool.model.rbac;

import java.util.HashSet;
import java.util.Set;

public class Subject extends RbacElement {
	
	public Subject(String name, RbacModel model) {
		super(name, model);
	}

	public Set<Role> roles = new HashSet<Role>();
	
	public Subject addRole(Role role) {
		this.roles.add(role);
		role.addSubject(this);
		
		return this;
	}
	
	public Set<Role> getRoles() {
		
		return this.roles;
	}
}
