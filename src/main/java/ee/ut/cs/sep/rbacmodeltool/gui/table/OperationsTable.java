package ee.ut.cs.sep.rbacmodeltool.gui.table;

import java.awt.Dimension;

import javax.swing.JTable;

import ee.ut.cs.sep.rbacmodeltool.gui.tablemodel.OperationsTableModel;

@SuppressWarnings("serial")
public class OperationsTable extends JTable {

	public OperationsTable(OperationsTableModel tasksTableModel) {
		this.setPreferredScrollableViewportSize(new Dimension(180, 70));
		this.setFillsViewportHeight(true);
		this.setModel(tasksTableModel);
	}
}
