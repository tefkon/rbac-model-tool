package ee.ut.cs.sep.rbacmodeltool.model.businessprocess.algorithms;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import ee.ut.cs.sep.rbacmodeltool.analysis.strategy.xes.AbstractXesHandler;
import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.BPActivity;
import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.BPAttribute;
import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.BPOriginator;
import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.ProcessModel;
import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.BPRole;

public class XesHandler extends AbstractXesHandler {
	
	private List<String> currentAttributes = new ArrayList<String>();
	private List<Object> currentAttributeValues = new ArrayList<Object>();
	
	protected ProcessModel processModel;
	
	public XesHandler(ProcessModel processModel) {
		this.processModel = processModel;
	}
		
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		
		super.startElement(uri, localName, qName, attributes);
		
		if (qName.equalsIgnoreCase(PROCESS_INSTANCE)) {
			processModel.incrementNumberOfCases();
		}
		
		if (qName.equalsIgnoreCase(EVENT)) {
			isEvent = true;
			processModel.incrementNumberOfEvents();
		}

		if (qName.equalsIgnoreCase(STRING_ELEMENT) || qName.equalsIgnoreCase(INT_ELEMENT)) {
			int attrLength = attributes.getLength();
			
			String value = null;
			
			String keyAttributeValue = null;
			String valueAttributeValue = null;
			
			for(int i = 0; i < attrLength; i++) {
				String key = attributes.getQName(i);
				String keyValue = attributes.getValue(i);
				
				if (key.equalsIgnoreCase(KEY_ATTRIBUTE)) {
					keyAttributeValue = keyValue;
				}
				
				if (key.equalsIgnoreCase(VALUE_ATTRIBUTE)) {
					valueAttributeValue = keyValue;
				}
				
				if (key.equalsIgnoreCase(KEY_ATTRIBUTE)
					&& keyValue.equalsIgnoreCase(CONCEPT_NAME)) {
					isConceptName = true;
				}
				
				if (key.equalsIgnoreCase(KEY_ATTRIBUTE)
					&& (keyValue.equalsIgnoreCase(ORG_RESOURCE) || keyValue.equalsIgnoreCase(RESOURCE))) {
					isOrgResource = true;
				}
				
				if (key.equalsIgnoreCase(KEY_ATTRIBUTE) &&
					(keyValue.equalsIgnoreCase(ORG_ROLE) ||
					keyValue.equalsIgnoreCase(ROLE))) {
					isOrgRole = true;
				}
				
				if (key.equalsIgnoreCase(KEY_ATTRIBUTE) &&
					keyValue.equalsIgnoreCase(LIFECYCLE_TRANSITION)) {
					isLifecycleTransition = true;
				}
				
				if (key.equalsIgnoreCase(VALUE_ATTRIBUTE)) {
					value = keyValue;
				}
			}
			
			if (isEvent && value != null) {
				if (isConceptName) {
					currentTask = value;
				} else if (isOrgResource) {
					currentSubject = value;
					addOriginator(value);
				} else if (isOrgRole) {
					currentRole = value;
					addRole(value);
				} else if (isLifecycleTransition) {
					currentLifecycleTransition = value;
				} else if (keyAttributeValue != null && valueAttributeValue != null) {
					//addDataAttribute(keyAttributeValue, valueAttributeValue);
					currentAttributes.add(keyAttributeValue);
					currentAttributeValues.add(valueAttributeValue);
				}
			}
		}
		
	}	
	
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		
		super.characters(ch, start, length);
	}
	
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		
		super.endElement(uri, localName, qName);
		
		if (qName.equalsIgnoreCase(EVENT)) {
			isEvent = false;
			if (currentSubject != null) {
				if (currentRole == null) {
					currentRole = "UNKNOWN";
					addRole(currentRole);
				}
				addSubjectToRole(currentSubject, currentRole);
			}
			
			if (currentTask != null) {
				String taskName = BPActivity.composeName(currentTask, currentLifecycleTransition);
				addActivity(taskName);
			}
			
			currentTask = null;
			currentLifecycleTransition = null;
			currentRole = null;
			currentSubject = null;
			currentAttributes.clear();
			currentAttributeValues.clear();
		}

		if (qName.equalsIgnoreCase(PROCESS_INSTANCE)) {
			numberOfInstances++;
		}
		
		resetDataElements();
	}

	
	public void addActivity(String name) {
		BPActivity activity = processModel.createOrGetActivity(name);
		processModel.addActivity(activity);
		
		for(int i = 0; i < currentAttributes.size(); i++) {
			BPAttribute attribute = this.addDataAttribute(currentAttributes.get(i), currentAttributeValues.get(i));
			attribute.addActivity(activity);
		}
		
		if (currentRole != null) {
			BPRole role = processModel.createOrGetRole(currentRole);
			activity.addRole(role);
		}
	}

	public void addOriginator(String name) {
		BPOriginator originator = processModel.createOrGetOriginator(name);
		processModel.addOriginator(originator);
	}

	public void addRole(String name) {
		BPRole role = processModel.createOrGetRole(name);
		processModel.addRole(role);
	}
	
	public void addSubjectToRole(String originatorName, String roleName) {
		BPOriginator originator = processModel.getOriginator(originatorName);
		BPRole role = processModel.createOrGetRole(roleName);
		role.addOriginator(originator);
	}

	public BPAttribute addDataAttribute(String attributeName, Object value) {
		BPAttribute attribute = processModel.createOrGetAttribute(attributeName);
		attribute.addValue(value);
		processModel.addAttribute(attribute);
		
		return attribute;
	}

	private void resetDataElements() {
		isOrgResource = false;
		isOrgRole = false;
		isConceptName = false;
		isLifecycleTransition = false;
	}
	
}
