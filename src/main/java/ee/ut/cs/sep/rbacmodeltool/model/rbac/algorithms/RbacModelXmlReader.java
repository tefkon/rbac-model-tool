package ee.ut.cs.sep.rbacmodeltool.model.rbac.algorithms;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import ee.ut.cs.sep.rbacmodeltool.io.CommonFile;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.RbacModel;

/**
 * XML reader for RBAC model in format of XML
 * 
 * @author Taivo
 */
public class RbacModelXmlReader {
	
	public static RbacModel read(CommonFile rbacModelFile) {
		RbacModel rbacModel = new RbacModel();
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser;
		RbacModelXmlHandler handler = new RbacModelXmlHandler(rbacModel);
		try {
			saxParser = factory.newSAXParser();
			saxParser.parse(rbacModelFile.getFile(), handler);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}	catch (IOException e) {
			e.printStackTrace();
		}
		
		return rbacModel;
	}

}
