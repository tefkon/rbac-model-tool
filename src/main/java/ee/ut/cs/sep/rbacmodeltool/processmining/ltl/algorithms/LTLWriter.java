package ee.ut.cs.sep.rbacmodeltool.processmining.ltl.algorithms;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import ee.ut.cs.sep.rbacmodeltool.io.ltl.LTLFile;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.Operation;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.RbacModel;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.ResourceAttribute;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.Role;
import ee.ut.cs.sep.rbacmodeltool.model.rbac.Subject;
import ee.ut.cs.sep.rbacmodeltool.processmining.ltl.LTLFormula;

/**
 * Writes for LTL formulas based on RBAC model constraints, specifically
 * role-permission assignments.
 */
public class LTLWriter {
	
	private HashMap<String, String> attributeAliases = new HashMap<String, String>();
	
	private int attrIndex = 1;
	
	public String getAttributeAlias(String attributeName) {
		
		return this.attributeAliases.get(attributeName);
	}
	
	public void addAttributeAlias(String attributeName) {
		attributeAliases.put(attributeName, "attr" + attrIndex);
		attrIndex++;
	}
	
	/**
	 * Writes RBAC model constraints as LTL formulas into file
	 * 
	 * @param rbacModel
	 */
	public void write(LTLFile ltlFile, RbacModel rbacModel) {
		for (ResourceAttribute resource : rbacModel.getResourceAttributes()) {
			this.addAttributeAlias(resource.getName());
		}
		
		try {
			PrintWriter writer = new PrintWriter(new FileWriter(ltlFile.getFile()));
			addDeclarations(writer);
			
			HashMap<ResourceAttribute, HashMap<Operation, HashSet<Role>>> rolePermissionAssignments = rbacModel.getRolePermissionAssignments();
			
			for (Entry<ResourceAttribute, HashMap<Operation, HashSet<Role>>> entry : rolePermissionAssignments.entrySet()) {
				String resourceName = entry.getKey().getName();
				for (Entry<Operation, HashSet<Role>> subEntry : entry.getValue().entrySet()) {
					Operation operation = subEntry.getKey();
					String operationName = operation.getName();
					
					HashSet<String> subjects = new HashSet<String>();
					for (Role role : subEntry.getValue()) {
						for (Subject subject : role.getSubjects()) {
							subjects.add(subject.getName());
						}
					}
					
					LTLFormula ltlFormula = createRolePermissionLTLFormula(resourceName, operationName, subjects);
					writer.println(ltlFormula.toString());
				}
			}
			
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Creates LTL formula content from RBAC resource, operation, and subjects
	 * 
	 * @param resourceName
	 * @param operationName
	 * @param subjects
	 * @return
	 */
	public LTLFormula createRolePermissionLTLFormula(String resourceName, String operationName, HashSet<String> subjects) {
		String attrName = getAttributeAlias(resourceName);
		
		String[] operationNameParts = operationName.split("\\\\n", 2); // Exclude transition type
		String formula = "";
		
		if (operationNameParts.length > 1) {
			formula = "(activity == \"" + operationNameParts[0] + "\" /\\ eventType == \"" + operationNameParts[1] +"\")";
		} else {
			formula = "activity == \"" + operationNameParts[0] + "\"";
		}
		
		formula = "(" + attrName + " != \"\" /\\ " + formula + ")";
		String subFormula = null;
		String prevSubFormula = null;
		for (String subjectName : subjects) {
			subFormula = "subject == \"" + subjectName + "\"";
			if (prevSubFormula != null) {
				subFormula = "( " + subFormula + " \\/ " +  prevSubFormula + " )";
			}
			prevSubFormula = subFormula;
		}
		
		// For some reason requires extra brackets
		formula = "[]((" + formula + " -> " + subFormula + "))";
		
		String formulaName = "RP_" + attrName + "_" + cleanString(operationName);
				
		return new LTLFormula(formulaName, formula);
	}
	
	public void addDeclarations(PrintWriter writer) {
		for (Entry<String, String> entry : attributeAliases.entrySet()) {
			// Assuming all are string attributes
			writer.println("string ate." + entry.getKey() + ";");
			writer.println("rename ate." + entry.getKey() + " as " + entry.getValue() + ";");
		}
		writer.println("set ate.EventType;");
		writer.println("set ate.Originator;");
		writer.println("set ate.WorkflowModelElement;");
		writer.println("rename ate.Originator as subject;");
		writer.println("rename ate.WorkflowModelElement as activity;");
		writer.println("rename ate.EventType as eventType;");
	}
	
	protected String cleanString(String string) {
		
		return string.replaceAll("\\\\n", " ")
			.replaceAll(" ", "_")
			.replaceAll("[^a-zA-Z0-9-_]", "");
	}
}
