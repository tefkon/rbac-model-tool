package ee.ut.cs.sep.rbacmodeltool.analysis.strategy.xes;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public abstract class AbstractXesHandler extends DefaultHandler {
	
	protected int numberOfInstances = 0;
	
	protected static final String EVENT 				= "event";
	protected static final String PROCESS_INSTANCE 		= "trace";
	protected static final String STRING_ELEMENT		= "string";
	protected static final String INT_ELEMENT			= "int";
	protected static final String DATE_ELEMENT			= "date";
	protected static final String BOOLEAN_ELEMENT		= "boolean";
	protected static final String FLOAT_ELEMENT			= "float";
	protected static final String KEY_ATTRIBUTE 		= "key";
	protected static final String VALUE_ATTRIBUTE 		= "value";
	protected static final String CONCEPT_NAME			= "concept:name";
	protected static final String ORG_RESOURCE			= "org:resource";
	protected static final String ORG_ROLE				= "org:role";
	protected static final String RESOURCE				= "resource";
	protected static final String ROLE					= "role";
	protected static final String TIME_TIMESTAMP 		= "time:timestamp";
	protected static final String LIFECYCLE_TRANSITION 	= "lifecycle:transition";

	protected boolean isEvent = false;
	protected boolean isConceptName = false;
	protected boolean isOrgResource = false;
	protected boolean isOrgRole = false;
	protected boolean isAttribute = false;
	protected boolean isLifecycleTransition = false;
	
	protected String currentSubject = null;
	protected String currentRole = null;
	protected String currentTask = null;
	protected String currentLifecycleTransition = null;
	protected String currentAttribute = null;
	
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		
		super.startElement(uri, localName, qName, attributes);
	}

	public void characters(char[] ch, int start, int length)
			throws SAXException {
		
		super.characters(ch, start, length);
	}

	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		
		super.endElement(uri, localName, qName);
	}

}
