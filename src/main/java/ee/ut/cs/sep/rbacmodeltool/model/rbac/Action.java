package ee.ut.cs.sep.rbacmodeltool.model.rbac;

public class Action {

	public static String CREATE = "Create";
	public static String READ  	= "Read";
	public static String UPDATE	= "Update";
	public static String DELETE	= "Delete";
	
}
