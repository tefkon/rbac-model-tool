package ee.ut.cs.sep.rbacmodeltool.gui.tablemodel;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.BPActivity;
import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.BPRole;
import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.ProcessModel;

@SuppressWarnings("serial")
public class OperationRolesTableModel extends AbstractTableModel {

	private String[] columnNames = { "Is selected", "Role name" };
	
	protected BPActivity selectedActivity = null;

	protected ProcessModel processModel;
	
	private List<Boolean> selected = new ArrayList<Boolean>();
	
	public OperationRolesTableModel(ProcessModel processModel) {
		this.processModel = processModel;
	}
	
	public int getColumnCount() {
		
		return columnNames.length;
	}

	public int getRowCount() {
		
		return processModel.getRoles().size();
	}

	public Object getValueAt(int row, int col) {
		if (col < 0) {
			
			return null;
		}
		
		BPRole role = processModel.getRoles().get(row);
		if (selectedActivity != null) {
			if (col == 0) {
				return (Boolean) selectedActivity.hasRole(role);
			} 
		}
		
		if (col == 1) {
			return role.getName();
		}
		
		return null;
		
		
	}
	
	public void setValueAt(Object value, int row, int col) {
		if (col == 0) {
			this.selected.set(row, (Boolean) value);
			if (this.selectedActivity != null) {
				BPRole role = processModel.getRoles().get(row);
				if ((Boolean) value) {
					selectedActivity.addRole(role);
				} else {
					selectedActivity.removeRole(role);
				}
			} 
		}
		
		fireTableCellUpdated(row, col);
	}
	
	public String getColumnName(int col) {
		
		return columnNames[col];
	}

	public boolean isCellEditable(int row, int col) {
		
		return col == 0;
	}
			
	public Class<?> getColumnClass(int col) {
		
		return (col == 0) ? Boolean.class : String.class;
	}
	
	public void updateTable() {
		super.fireTableDataChanged();
	}
	
	public void updateTable(BPActivity activity) {
		if (processModel.getRoles().size() == 0) return;
		
		this.selectedActivity = activity;
		for (int i = 0; i < processModel.getRoles().size(); i++) {
			selected.add(i, Boolean.valueOf(activity.hasRole(processModel.getRoles().get(i))));
		}
		
		super.fireTableDataChanged();
	}
	
}
