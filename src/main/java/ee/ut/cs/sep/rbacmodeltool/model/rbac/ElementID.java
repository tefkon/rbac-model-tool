package ee.ut.cs.sep.rbacmodeltool.model.rbac;

import java.util.concurrent.atomic.AtomicInteger;

public class ElementID {

	private final AtomicInteger counter = new AtomicInteger();
	private String name;
	private int id;
	
	public ElementID() {
		this(null);
	}
	
	public ElementID(String name) {
		id = getNextUniqueIndex();
		if (name != null) {
			this.name = name;
		} else {
			this.name = generateUniqueName(id);
		}
	}
	
	public int getId() {
		
		return this.id;
	}
	
	public String getName() {
		
		return this.name;
	}
	
	/**
	 * Hash code of element's name
	 */
	public int hashCode() {
		
		return name.hashCode();
	}
	
	public String toString() {
		
		return this.getName();
	}
	
	protected String generateUniqueName(int id) {
//		UUID uuid = UUID.randomUUID();
//		return uuid.toString();
		return "ID" + id;
	}
	
	protected int getNextUniqueIndex() {
		
		return counter.incrementAndGet();
	}
}
