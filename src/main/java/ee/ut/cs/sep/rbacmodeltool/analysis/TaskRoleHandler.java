package ee.ut.cs.sep.rbacmodeltool.analysis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class TaskRoleHandler extends DefaultHandler {

	private int numberOfInstances = 0;
	private Set<String> tasks = new HashSet<String>();
	private Set<String> originators = new HashSet<String>();
	private HashMap<String, Set<String>> dataAttributes = new HashMap<String, Set<String>>();
	
	// Task - Originator - Number of occurences
	private HashMap<String, HashMap<String, Integer>> taskOriginatorMapping
		= new HashMap<String, HashMap<String, Integer>>();
	
	private HashMap<String, HashMap<String, Integer>> taskDependencyMapping
		= new HashMap<String, HashMap<String, Integer>>();
	
	private boolean isTask = false;
	private boolean isOriginator = false;
	private boolean isData = false;	
	
	private ArrayList<String> currentInstanceTasks = new ArrayList<String>();
	private String currentTask = null;
	private String currentOriginator = null;
	private String currentDataAttribute = null;
	
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		
		super.startElement(uri, localName, qName, attributes);
		
		if (qName.equalsIgnoreCase(Parser.TASK)) {
			this.isTask = true;
		}
		
		if (qName.equalsIgnoreCase(Parser.ORIGINATOR)) {
			this.isOriginator = true;
		}
		
		if (qName.equalsIgnoreCase(Parser.DATA_ATTRIBUTE)) {
//			int length = attributes.getLength();
			
			/**
			 * In here data attributes could be different.
			 * They can be related to business process or the artifacts used within business process
			 */
//			for(int i = 0; i < length; i++) {
//				String name = attributes.getQName(i);
//				String name2 = attributes.getLocalName(i);
//				System.out.println("Name: " + name);
//				System.out.println("LocalName: " + name2);
//				System.out.println(attributes.getValue(i));
//				
//				currentDataAttribute = attributes.getValue(i);
//			}
//			
			this.currentDataAttribute = attributes.getValue(0);
			this.isData = true;
		}
	}
	
	public void characters(char ch[], int start, int length) throws SAXException {
		String contents = new String(ch, start, length);
		
		addTask(contents);
		addOriginator(contents);
		addData(contents);
	}
	
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		
		super.endElement(uri, localName, qName);
		
		if (qName.equalsIgnoreCase(Parser.PROCESS_INSTANCE)) {
			this.numberOfInstances++;
			updateStatistics();
			currentInstanceTasks.clear();
		}
	}
	
	public Set<String> getTasks() {
		
		return this.tasks;
	}
	
	public Set<String> getOriginators() {
		
		return this.originators;
	}
	
	public int getNumberOfInstances() {
		
		return this.numberOfInstances;
	}
	
	public HashMap<String, HashMap<String, Integer>> getTaskOriginatorMapping() {
		
		return this.taskOriginatorMapping;
	}
	
	public HashMap<String, HashMap<String, Integer>> getTaskDependencyMapping() {
		return taskDependencyMapping;
	}
	
	public HashMap<String, Set<String>> getDataAttributes() {
		
		return dataAttributes;
	}

	private void updateStatistics() {
		int total = currentInstanceTasks.size();
		for (int i = 0; i < total; i++) {
			for (int j = i+1; j < total; j++) {
				String taskFrom = currentInstanceTasks.get(i);
				String taskTo = currentInstanceTasks.get(j);
				
				//System.out.println(taskFrom + " => " + taskTo);
				HashMap<String, Integer> task = new HashMap<String, Integer>();
				task.put(taskTo, 1);
				if (this.taskDependencyMapping.containsKey(taskFrom)) {
					task = this.taskDependencyMapping.get(taskFrom);
					if (task.containsKey(taskTo)) {
						int nr = task.get(taskTo).intValue() + 1;
						task.put(taskTo, nr);
					} else {
						task.put(taskTo, 1);
					}
				} else {
					taskDependencyMapping.put(taskFrom, task);
				}
			}
		}
	}
	
	private void addTask(String contents) {
		if (this.isTask) {
			if (!this.tasks.contains(contents)) {
				this.tasks.add(contents);
			}
			this.currentTask = contents;
			this.isTask = false;
			
			currentInstanceTasks.add(contents);
		}
	}
	
	private void addOriginator(String contents) {
		if (this.isOriginator) {
			this.currentOriginator = contents;
			if (!this.originators.contains(contents)) {
				this.originators.add(contents);
			}
			
			if (this.currentTask != null) {
				if (this.taskOriginatorMapping.containsKey(this.currentTask)) {
					HashMap<String, Integer> task = this.taskOriginatorMapping.get(this.currentTask);
					
					if (task.containsKey(this.currentOriginator)) {
						Integer occurances = task.get(this.currentOriginator);
						int nr = occurances.intValue() + 1;
						task.put(this.currentOriginator, nr);
					} else {
						task.put(this.currentOriginator, 1);
					}
					
				} else {
					HashMap<String, Integer> task = new HashMap<String, Integer>();
					task.put(this.currentOriginator, 1);
					this.taskOriginatorMapping.put(this.currentTask, task);
					this.currentOriginator = null;
					this.currentTask = null;
				}
			}
			
			this.isOriginator = false;
		}
	}
	
	private void addData(String contents) {
		if (this.isData) {
			if (this.currentDataAttribute != null) {
				Set<String> values = new HashSet<String>();
				if (this.dataAttributes.containsKey(this.currentDataAttribute)) {
					values = this.dataAttributes.get(this.currentDataAttribute);
					values.add(contents);
 				} else {
 					values.add(contents);
				}
				
				this.dataAttributes.put(this.currentDataAttribute,  values);
			}
			
			this.isData = false;
		}
	}
}
