package ee.ut.cs.sep.rbacmodeltool.model.businessprocess;

import java.util.HashSet;
import java.util.Set;

/**
 * Role - a role executing activites, usually a predefined job function within an organization
 */
public class BPRole {

	private String name;
	private Set<BPOriginator> originators = new HashSet<BPOriginator>();
	
	public BPRole(String name) {
		this.name = name;
	}
	
	public String getName() {
		
		return this.name;
	}
	
	public BPRole setName(String name) {
		this.name = name;
		
		return this;
	}
	
	public BPRole addOriginator(BPOriginator originator) {
		this.originators.add(originator);
		originator.addRole(this);
		
		return this;
	}
	
	public BPRole removeOriginator(BPOriginator originator) {
		originator.getRoles().remove(this);
		originators.remove(originator);
		
		return this;
	}
	
	public Set<BPOriginator> getOriginators() {
		
		return this.originators;
	}
}
