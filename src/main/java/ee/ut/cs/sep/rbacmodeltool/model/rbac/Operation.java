package ee.ut.cs.sep.rbacmodeltool.model.rbac;

public class Operation extends RbacElement {
	
	public Operation(String name, RbacModel model) {
		super(name, model);
	}

	public ResourceAttribute resource;
	
	public Operation setResourceAttribute(ResourceAttribute resource) {
		this.resource = resource;
		
		return this;
	}
	
	public ResourceAttribute getResourceAttribute() {
		
		return this.resource;
	}
	
}
