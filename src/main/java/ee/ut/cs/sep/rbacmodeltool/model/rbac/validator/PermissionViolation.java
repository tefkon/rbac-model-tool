package ee.ut.cs.sep.rbacmodeltool.model.rbac.validator;

public class PermissionViolation {
	
	protected String roleName;
	protected String operationName;
	protected String resourceName;
	protected String reason;
	protected int nrOccurances;
	protected boolean isKeep;
	
	public PermissionViolation(String roleName, String operationName, String resourceName, String reason) {
		this.roleName = roleName;
		this.operationName = operationName;
		this.resourceName = resourceName;
		this.reason = reason;
		this.isKeep = true;
		this.nrOccurances = 1;
	}

	public String getRoleName() {
		
		return roleName;
	}

	public String getOperationName() {
		
		return operationName;
	}

	public String getResourceName() {
		
		return resourceName;
	}

	public String getReason() {
		
		return reason;
	}

	public boolean isKeep() {
		
		return isKeep;
	}

	public PermissionViolation setKeep(boolean isKeep) {
		this.isKeep = isKeep;
		
		return this;
	}
	
	public PermissionViolation increaseNrOccurances() {
		
		return this.increaseNrOccurances(1);
	}
	
	public PermissionViolation increaseNrOccurances(int incrementNumber) {
		this.nrOccurances += incrementNumber;
		
		return this;
	}
	
	public int getNrOccurances() {
		
		return this.nrOccurances;
	}
}
