package ee.ut.cs.sep.rbacmodeltool.model.businessprocess;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Activity - a predefined step in the process
 */
public class BPActivity {

	private String name;
	
	private HashMap<String, BPRole> roles = new HashMap<String, BPRole>();
	
	public BPActivity(String name) {
		this.name = name;
	}
	
	public String getName() {
		
		return this.name;
	}
	
	public BPActivity setName(String name) {
		this.name = name;
		
		return this;
	}
	
	public BPActivity addRole(BPRole role) {
		this.roles.put(role.getName(), role);
		
		return this;
	}
	
	public BPActivity removeRole(BPRole role) {
		this.roles.remove(role);
		
		return this;
	}
	
	public List<BPRole> getRoles() {
		List<BPRole> result = new ArrayList<BPRole>();
		for (BPRole role : this.roles.values()) {
			result.add(role);
		}
		
		return result;
	}
	
	public boolean hasRole(BPRole role) {
		
		return this.hasRole(role.getName());
	}
	
	public boolean hasRole(String roleName) {
		
		return this.roles.containsKey(roleName);
	}
 	
	/**
	 * This format is required in ProM 5.2, however it could be a bug in ProM
	 * 5.2 to use as separator "\\n".
	 * 
	 * @param activityName
	 * @param transitionName
	 * @return
	 */
	public static String composeName(String activityName, String transitionName) {
		String name = activityName;
		if (transitionName != null) {
			name += "\\n" + transitionName;
		}
		return name;
	}
	
}
