package ee.ut.cs.sep.rbacmodeltool.importing.businessprocess;

import java.io.File;

import javax.swing.filechooser.FileFilter;

import ee.ut.cs.sep.rbacmodeltool.importing.AbstractImport;
import ee.ut.cs.sep.rbacmodeltool.io.businessprocess.LogFile;

/**
 * XES import holds the reference to the import log file
 * 
 * @author Taivo
 */
public class XesImport extends AbstractImport {
	
	public FileFilter getFileFilter() {
		if (fileFilter == null) {
			fileFilter = new FileFilter() {
				public boolean accept(File file) {
					boolean valid = file.getName().endsWith(".xes");
					
					/**
					 * boolean valid = file.getName().endsWith(".txt")
							|| file.getName().endsWith(".xes")
							|| file.getName().endsWith(".mxml")
							|| file.getName().endsWith(".xml");
						
						return file.isDirectory() || valid && file.isFile();
					 */
					return file.isDirectory() || valid && file.isFile();
				}
				
				public String getDescription() {
					return "XES (*.xes)";
				}
			};
		}
		
		return fileFilter;
	}

	public void importFile(File file) {
		this.file = new LogFile(file);
	}
	
}
