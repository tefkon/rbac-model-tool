package ee.ut.cs.sep.rbacmodeltool.util;

import java.util.List;

public class StringUtil {
	
	public static String concatenate(List<String> list) {
		
		return concatenate(list, ", ");
	}
	
	public static String concatenate(List<String> list, String separator) {
		String result = "";
		for (int i = 0; i < list.size(); i++) {
			result += list.get(i);
			if (i != (list.size() - 1)) {
				result += separator;
			}
		}
		return result;
	}
}
