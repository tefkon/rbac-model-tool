package ee.ut.cs.sep.rbacmodeltool.analysis;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.swing.table.AbstractTableModel;

import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.BPActivity;

@SuppressWarnings("serial")
public class TasksTableModel  extends AbstractTableModel {

	static final String COLUMN_TASK = "Task name";
	protected List<String> columnNames = new ArrayList<String>();
	protected List<List<Object>> data = new ArrayList<List<Object>>();
	
	public TasksTableModel() {
		columnNames.add(COLUMN_TASK);
	}
	
	public int getColumnCount() {
		
		return columnNames.size();
	}

	public int getRowCount() {
		
		return data.size();
	}

	public Object getValueAt(int row, int col) {
		
		return data.get(row).get(col);
	}

	public void setValueAt(Object value, int row, int col) {
		data.get(row).set(col, value);
		fireTableCellUpdated(data.size(), data.size());
	}
	
	public String getColumnName(int col) {
		
		return columnNames.get(col);
	}

	public boolean isCellEditable(int row, int col) {

		return false;
	}
	
	public Class<?> getColumnClass(int col) {
		
		return String.class;
	}
	
	public void addRow(List<Object> list) {
		data.add(list);
		fireTableRowsInserted(data.size() - 1, data.size() - 1);
	}
	
	public void setActivities(Set<BPActivity> activites) {
		for (BPActivity activity : activites) {
			List<Object> list = new ArrayList<Object>();
			list.add(activity.getName());
			this.addRow(list);
		}
	}
}
