package ee.ut.cs.sep.rbacmodeltool.model.rbac;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ResourceAttribute extends RbacElement {
	
	public ResourceAttribute(String name, RbacModel model) {
		super(name, model);
	}

	public Set<Object> values = new HashSet<Object>();

	public HashMap<String, Operation> operations = new HashMap<String, Operation>();
	
	public ResourceAttribute addValue(Object value) {
		this.values.add(value);
		
		return this;
	}
	
	public ResourceAttribute setValues(Set<Object> values) {
		this.values = values;
		
		return this;
	}
	
	public Set<Object> getValues() {
		
		return this.values;
	}
	
	public ResourceAttribute addOperation(Operation operation) {
		this.operations.put(operation.getName(), operation);
		
		return this;
	}
	
	public List<Operation> getOperations() {
		List<Operation> result = new ArrayList<Operation>();
		for (Operation operation : this.operations.values()) {
			result.add(operation);
		}
		
		return result;
	}
	
}
