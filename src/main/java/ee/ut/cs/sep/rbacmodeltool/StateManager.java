package ee.ut.cs.sep.rbacmodeltool;

public class StateManager {
	
	private StateEnum state = StateEnum.STATE_START;
	
	public void updateState(StateEnum state) {
		this.state = state;
	}
	
	public StateEnum getState() {
		
		return this.state;
	}
}
