package ee.ut.cs.sep.rbacmodeltool;

import java.awt.Dimension;

import javax.swing.JFrame;

import ee.ut.cs.sep.rbacmodeltool.config.Config;
import ee.ut.cs.sep.rbacmodeltool.gui.MainFrame;

public class Application {
	
	private StateManager stateManager;
	
	public Application() {
		this.stateManager = new StateManager();
	}
	
	public void initialize() {
		MainFrame frame = new MainFrame(new ViewModel(this));
		frame.initialize();
		frame.setTitle(Config.getProperty("application.name"));
		frame.setSize(900,550);
		frame.setMinimumSize(new Dimension(400, 400));
		frame.setResizable(true);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public StateManager getStateManager() {
		return stateManager;
	}
	
}
