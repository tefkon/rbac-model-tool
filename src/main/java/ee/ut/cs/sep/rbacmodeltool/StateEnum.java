package ee.ut.cs.sep.rbacmodeltool;

public enum StateEnum {
	STATE_START,
	STATE_LOG_IMPORTED,
	STATE_LOG_ANALYZED,
	STATE_RBAC_MODEL_CREATED,
	STATE_BASE_RBAC_MODEL_IMPORTED /* Maybe not needed at all, does not affect */
}
