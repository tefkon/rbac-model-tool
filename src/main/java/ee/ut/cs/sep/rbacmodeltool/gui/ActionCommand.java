package ee.ut.cs.sep.rbacmodeltool.gui;

public class ActionCommand {
	
	public static String OPEN_FILE 		= "open-file";
	public static String EXIT 			= "exit";
	public static String EXPORT_XML 	= "export-xml";
	public static String IMPORT_XML 	= "import-xml";
	public static String ANALYZE_LOG 	= "analyze-file";
	public static String ADD_ROLE 		= "add-role";
	public static String REMOVE_ROLE 	= "remove-role";
	public static String CREATE_RBAC 	= "create-rbac";
	public static String VALIDATE 		= "validate";
	public static String EXPORT_RBAC	= "export-rbac";
	public static String RESET			= "reset";
	public static String MERGE_EXPORT	= "merge-export";
	public static String EXPORT_LTL  	= "export-ltl";
}
