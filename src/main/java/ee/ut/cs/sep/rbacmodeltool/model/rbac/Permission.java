package ee.ut.cs.sep.rbacmodeltool.model.rbac;

/**
 * Permission is by definition Operation + Resource
 * 
 * @author Taivo
 *
 */
public class Permission extends RbacElement {
	
	protected Operation operation;
	
	protected ResourceAttribute resource;
		
	public Permission(RbacModel model, Operation operation, ResourceAttribute resource) {
		super(resource.getName() + "." + operation.getName(), model);
		this.operation = operation;
		this.resource = resource;
	}

	public Operation getOperation() {
		return operation;
	}

	public ResourceAttribute getResource() {
		return resource;
	}
	
}
