package ee.ut.cs.sep.rbacmodeltool.model.businessprocess;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class ProcessModel {
	
	HashMap<String, BPOriginator> originators = new HashMap<String, BPOriginator>();
	HashMap<String, BPActivity> activities = new HashMap<String, BPActivity>();
	HashMap<String, BPRole> roles = new LinkedHashMap<String, BPRole>();
	HashMap<String, BPAttribute> attributes = new HashMap<String, BPAttribute>();
	
	private int numberOfCases = 0;
	private int numberOfEvents = 0;
	
	public BPOriginator getOriginator(String name) {
		
		return this.originators.get(name);
	}

	public ProcessModel addOriginator(BPOriginator originator) {
		this.originators.put(originator.getName(), originator);
		
		return this;
	}
	
	public BPActivity getActivity(String name) {
		
		return this.activities.get(name);
	}
	
	public ProcessModel addActivity(BPActivity activity) {
		this.activities.put(activity.getName(), activity);
		
		return this;
	}
	
	public BPRole getRole(String name) {
		
		return this.roles.get(name);
	}
	
	public ProcessModel addRole(BPRole role) {
		this.roles.put(role.getName(), role);
		
		return this;
	}
	
	public ProcessModel removeRole(int index) {
		List<BPRole> roles = this.getRoles();
		BPRole role = roles.get(index);
		
		return this.removeRole(role);
	}
	
	public ProcessModel removeRole(BPRole role) {
		this.roles.remove(role.getName());
		
		return this;
	}

	public BPAttribute getAttribute(String name) {
		
		return this.attributes.get(name);
	}
	
	public ProcessModel addAttribute(BPAttribute attribute) {
		this.attributes.put(attribute.getName(), attribute);
		
		return this;
	}
	
	/**
	 * Creates or get role with this name
	 * 
	 * @param name
	 * @return {@link BPRole}
	 */
	public BPRole createOrGetRole(String name) {
		BPRole role = this.getRole(name);
		if (role != null) {
			return role;
		}
		
		return new BPRole(name);
	}
	
	/**
	 * Creates or gets activity with this name
	 * 
	 * @param name
	 * @return {@link BPActivity}
	 */
	public BPActivity createOrGetActivity(String name) {
		BPActivity activity = this.getActivity(name);
		if (activity != null) {
			return activity;
		}
		
		return new BPActivity(name);
	}
	
	/**
	 * Creates or gets originator with this name
	 * 
	 * @param name
	 * @return {@link BPOriginator}
	 */
	public BPOriginator createOrGetOriginator(String name) {
		BPOriginator originator = this.getOriginator(name);
		if (originator != null) {
			return originator;
		}
		
		return new BPOriginator(name);
	}
	
	/**
	 * Creates or gets attribute with this name
	 * 
	 * @param name
	 * @return {@link BPAttribute}
	 */
	public BPAttribute createOrGetAttribute(String name) {
		BPAttribute attribute = this.getAttribute(name);
		if (attribute != null) {
			return attribute;
		}
		
		return new BPAttribute(name);
	}
	
	public List<BPRole> getRoles() {
		List<BPRole> result = new ArrayList<BPRole>();
		for (BPRole role : this.roles.values()) {
			result.add(role);
		}
		
		return result;
	}
	
	public List<BPOriginator> getOriginators() {
		List<BPOriginator> result = new ArrayList<BPOriginator>();
		for (BPOriginator originator : this.originators.values()) {
			result.add(originator);
		}
		
		return result;
	}
	
	public List<String> getOriginatorNames() {
		List<String> result = new ArrayList<String>();
		for (BPOriginator originator : this.originators.values()) {
			result.add(originator.getName());
		}
		
		return result;
	}
	
	public List<BPActivity> getActivities() {
		List<BPActivity> result = new ArrayList<BPActivity>();
		for (BPActivity activity : this.activities.values()) {
			result.add(activity);
		}
		
		return result;
	}
	
	public List<BPAttribute> getAttributes() {
		List<BPAttribute> result = new ArrayList<BPAttribute>();
		for (BPAttribute attribute : this.attributes.values()) {
			result.add(attribute);
		}
		
		return result;
	}
	
	public Boolean hasAttribute(BPAttribute attribute) {
		
		return hasAttribute(attribute.getName());
	}
	
	public Boolean hasAttribute(String attributeName) {
		
		return Boolean.valueOf(this.attributes.containsKey(attributeName));
	}
	
	public void incrementNumberOfEvents() {
		this.numberOfEvents++;
	}
	
	public int getNumberOfEvents() {
		
		return this.numberOfEvents;
	}
	
	public void incrementNumberOfCases() {
		this.numberOfCases++;
	}
	
	public int getNumberOfCases() {
		
		return this.numberOfCases;
	}
	
	public void reset() {
		this.activities.clear();
		this.attributes.clear();
		this.roles.clear();
		this.originators.clear();
	}
}
