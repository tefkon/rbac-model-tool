package ee.ut.cs.sep.rbacmodeltool.processmining.ltl;

public class LTLFormula {
	
	private String name;
	private String content;

	public LTLFormula(String name, String content) {
		this.name = name;
		this.content = content;
	}
	
	public String toString() {
		String lineSeparator = System.getProperty("line.separator");
		
		return "formula " + this.name + " () := {}" + lineSeparator
			+ this.content + ";";
	}
	
}
