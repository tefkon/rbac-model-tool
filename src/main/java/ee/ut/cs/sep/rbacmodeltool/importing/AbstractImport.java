package ee.ut.cs.sep.rbacmodeltool.importing;

import java.io.File;

import javax.swing.filechooser.FileFilter;

import ee.ut.cs.sep.rbacmodeltool.io.CommonFile;

public abstract class AbstractImport {

	protected CommonFile file = null;

	protected FileFilter fileFilter = null;
	
	/**
	 * File filter for specified files
	 * @return
	 */
	public abstract FileFilter getFileFilter();
	
	public abstract void importFile(File file);
	
	public CommonFile getCommonFile() {
		
		return file;
	}
	
}
