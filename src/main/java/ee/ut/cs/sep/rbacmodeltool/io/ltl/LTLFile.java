package ee.ut.cs.sep.rbacmodeltool.io.ltl;

import java.io.File;

import javax.swing.filechooser.FileFilter;

import ee.ut.cs.sep.rbacmodeltool.io.CommonFile;

/**
 * Encapsulate LTL file with extension ".ltl"
 * @author Taivo
 */
public class LTLFile extends CommonFile {
	
	public static FileFilter fileFilter = null;
	
	public LTLFile(File file) {
		super(file);
	}

	public String getFileExtension() {
		
		return "ltl";
	}
	
	public static FileFilter getFileFilter() {
		if (fileFilter == null) {
			fileFilter = new FileFilter() {
				public boolean accept(File file) {
					boolean valid = file.getName().endsWith(".ltl");
					
					/**
					 * boolean valid = file.getName().endsWith(".txt")
							|| file.getName().endsWith(".xes")
							|| file.getName().endsWith(".mxml")
							|| file.getName().endsWith(".xml");
						
						return file.isDirectory() || valid && file.isFile();
					 */
					return file.isDirectory() || valid && file.isFile();
				}
				
				public String getDescription() {
					return "LTL (*.ltl)";
				}
			};
		}
		
		return fileFilter;
	}
}
