package ee.ut.cs.sep.rbacmodeltool.model.businessprocess.algorithms;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;
import ee.ut.cs.sep.rbacmodeltool.io.CommonFile;
import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.ProcessModel;

/**
 * Log reader for event log file in XES format
 * 
 * @author Taivo
 */
public class XesReader {
		
	public static ProcessModel read(CommonFile logFile, ProcessModel processModel) {
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			// MxmlHandler handler = new MxmlHandler();
			// saxParser.parse(file, handler);
			XesHandler handler = new XesHandler(processModel);
			saxParser.parse(logFile.getFile(), handler);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return processModel;
	}
	
}
