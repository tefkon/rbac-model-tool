package ee.ut.cs.sep.rbacmodeltool.gui.tablemodel;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.BPOriginator;
import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.BPRole;
import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.ProcessModel;

@SuppressWarnings("serial")
public class RolesTableModel extends AbstractTableModel {

	public static final String COLUMN_HAS_ROLE = "Has role";
	public static final String COLUMN_ROLE_NAME = "Role name";
	
	private List<String> columnNames = new ArrayList<String>();
	private List<Boolean> selected = new ArrayList<Boolean>();
	protected ProcessModel processModel;
	
	protected BPOriginator selectedOriginator = null;
	
	public RolesTableModel(ProcessModel processModel) {
		this.processModel = processModel;
		
		columnNames.add(COLUMN_HAS_ROLE);
		columnNames.add(COLUMN_ROLE_NAME);
	}
	
	public int getColumnCount() {
		
		return columnNames.size();
	}

	public int getRowCount() {
		
		return processModel.getRoles().size();
	}

	public Object getValueAt(int row, int col) {
		if (col < 0) {
			return null;
		}
		
		if (col == 0) {
			
			return (Boolean) selected.get(row);
		} 
		
		return processModel.getRoles().get(row).getName();
	}
	
	public void setValueAt(Object value, int row, int col) {
		
		if (col == 0) {
			this.selected.set(row, (Boolean) value);
			
			if (this.selectedOriginator != null) {
				if ((Boolean) value) {
					this.processModel.getRoles().get(row).addOriginator(this.selectedOriginator);
				} else {
					this.processModel.getRoles().get(row).removeOriginator(this.selectedOriginator);
				}
			} 
		} else {
			this.processModel.getRoles().get(row).setName((String) value);
		}
		
		fireTableCellUpdated(row, col);
	}
	
	public String getColumnName(int col) {
		
		return columnNames.get(col);
	}

	public boolean isCellEditable(int row, int col) {

		return true;
	}
	
	public void addRow(List<Object> list) {
		selected.add((Boolean) list.get(0));
		BPRole role = processModel.createOrGetRole("Role" + processModel.getRoles().size());
		processModel.addRole(role);
		
		fireTableRowsInserted(processModel.getRoles().size() - 1, processModel.getRoles().size() - 1);
	}
	
	public void removeRow(int row) {
		selected.remove(row);
		processModel.removeRole(row);
		
		fireTableRowsDeleted(row, row);
	}
		
	public Class<?> getColumnClass(int col) {
		
		return (col == 0) ? Boolean.class : String.class;
	}
	
	public void updateTable() {
		if (processModel.getRoles().size() == 0) return;
		for (int i = 0; i < processModel.getRoles().size(); i++) {
			selected.add(i, new Boolean(false));
		}
		super.fireTableDataChanged();
	}
	
	public void updateTable(BPOriginator originator) {
		if (processModel.getRoles().size() == 0) return;
		
		this.selectedOriginator = originator;
		for (int i = 0; i < processModel.getRoles().size(); i++) {
			selected.add(i, Boolean.valueOf(originator.hasRole(processModel.getRoles().get(i))));
		}
		
		super.fireTableDataChanged();
	}
	
}