package ee.ut.cs.sep.rbacmodeltool.model.businessprocess;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Originator - a subject or a system executing activities
 */
public class BPOriginator {

	private String name;
	
	private HashMap<String, BPRole> roles = new HashMap<String, BPRole>();
	
	public BPOriginator(String name) {
		this.name = name;
	}
	
	public String getName() {
		
		return this.name;
	}
	
	public BPOriginator addRole(BPRole role) {
		this.roles.put(role.getName(), role);
		
		return this;
	}
	
	public List<BPRole> getRoles() {
		List<BPRole> result = new ArrayList<BPRole>();
		
		for (BPRole role : this.roles.values()) {
			result.add(role);
		}
		
		return result;
	}
	public boolean hasRole(BPRole role) {
		
		return this.hasRole(role.getName());
	}
	
	public boolean hasRole(String roleName) {
		
		return this.roles.containsKey(roleName);
	}
	
}
