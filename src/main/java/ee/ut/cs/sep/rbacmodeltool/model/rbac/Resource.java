package ee.ut.cs.sep.rbacmodeltool.model.rbac;

import java.util.ArrayList;

public class Resource extends RbacElement {
	
	public Resource(String name, RbacModel model) {
		super(name, model);
	}

	public ArrayList<ResourceAttribute> attributes = new ArrayList<ResourceAttribute>();
	
	public ArrayList<Operation> operations = new ArrayList<Operation>();
	
	public Resource addAttribute(ResourceAttribute attribute) {
		this.attributes.add(attribute);
		
		return this;
	}
	
	public ArrayList<ResourceAttribute> getAttributes() {
		
		return this.attributes;
	}
	
	public Resource addOperation(Operation operation) {
		this.operations.add(operation);
		
		return this;
	}

	public ArrayList<Operation> getOperations() {
		
		return this.operations;
	}
	
}
