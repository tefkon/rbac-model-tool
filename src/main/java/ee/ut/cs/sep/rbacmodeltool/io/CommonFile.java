package ee.ut.cs.sep.rbacmodeltool.io;

import java.io.File;

/**
 * Encapsulates file, ensures that file has the correct file extension
 * @author Taivo
 */
public abstract class CommonFile implements FileExtensionAware {

	protected File file;
	
	public CommonFile(File file) {
		this.file = file;
	}
	
	public File getFile() {
		
		return file;
	}
	
	public String getPath() {
		String path = file.getPath();
		if (hasCorrectExtension(path)) {
			
			return path;
		}
		
		return path + "." + getFileExtension();
	}
	
	protected boolean hasCorrectExtension(String fileName) {
		return !(file.getName().endsWith(getFileExtension()));
	}
	
}
