package ee.ut.cs.sep.rbacmodeltool.model.rbac;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.BPActivity;
import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.BPAttribute;
import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.BPOriginator;
import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.BPRole;
import ee.ut.cs.sep.rbacmodeltool.model.businessprocess.ProcessModel;
import ee.ut.cs.sep.rbacmodeltool.processmining.ltl.LTLFormula;
import ee.ut.cs.sep.rbacmodeltool.processmining.ltl.algorithms.LTLWriter;

public class RbacModelConverter {
	
	public RbacModel createRbacModelFromProcessModel(ProcessModel processModel) {
		RbacModel rbacModel = new RbacModel();
		
		for (BPActivity bpActivity : processModel.getActivities()) {
			Operation operation = rbacModel.createOrGetOperation(bpActivity.getName());
			rbacModel.addOperation(operation);
		}
		
		for (BPOriginator bpOriginator : processModel.getOriginators()) {
			Subject subject = rbacModel.createOrGetSubject(bpOriginator.getName());
			rbacModel.addSubject(subject);
		}
	
		for (BPRole bpRole : processModel.getRoles()) {
			Role role = rbacModel.createOrGetRole(bpRole.getName());
			rbacModel.addRole(role);
			
			for (BPOriginator bpOriginator : bpRole.getOriginators()) {
				Subject subject = rbacModel.getSubject(bpOriginator.getName());
				role.addSubject(subject);
			}
		}
		
		for (BPAttribute bpAttribute : processModel.getAttributes()) {
			if (bpAttribute.isSelected()) {
				ResourceAttribute attribute = rbacModel.createOrGetResourceAttribute(bpAttribute.getName());
				attribute.setValues(bpAttribute.getValues());
				rbacModel.addResourceAttribute(attribute);
				
				for (BPActivity bpActivity : bpAttribute.getActivities()) {
					Operation operation = rbacModel.getOperation(bpActivity.getName());
					attribute.addOperation(operation);
				}
			}
		}
		
		for (BPAttribute bpAttribute : processModel.getAttributes()) {
			if (bpAttribute.isSelected()) {
				ResourceAttribute resource = rbacModel.getResourceAttribute(bpAttribute.getName());
				
				for (BPActivity bpActivity : bpAttribute.getActivities()) {
					Operation operation = rbacModel.getOperation(bpActivity.getName());
					
					for (BPRole bpRole : bpActivity.getRoles()) {
						Role role = rbacModel.getRole(bpRole.getName());
						
						Permission permission = rbacModel.createOrGetPermission(resource, operation);
						if (!role.hasPermission(permission)) {
							
//							if (role.getName().equals("A2_4")) {
//								System.out.println("Adding permission for A2_4: " + resource.getName() + " " + operation.getName());
//							}
							role.addPermission(permission);
						}
					}
				}
			}
		}
		
		return rbacModel;
	}

	public ProcessModel createProcessModelFromRbacModel(ProcessModel processModel, RbacModel rbacModel) {
		for (Operation operation : rbacModel.getOperations()) {
			BPActivity activity = processModel.createOrGetActivity(operation.getName());
			processModel.addActivity(activity);
		}
		
		for (Subject subject : rbacModel.getSubjects()) {
			BPOriginator originator = processModel.createOrGetOriginator(subject.getName());
			processModel.addOriginator(originator);
		}
		
		for (Role role : rbacModel.getRoles()) {
			BPRole bpRole = processModel.createOrGetRole(role.getName());
			processModel.addRole(bpRole);
			
			for (Subject subject : role.getSubjects()) {
				BPOriginator originator = processModel.getOriginator(subject.getName());
				bpRole.addOriginator(originator);
			}
			
			for (Permission permission : role.getPermissions()) {
				BPActivity activity = processModel.getActivity(permission.getOperation().getName());
				activity.addRole(bpRole);
			}
		}
		
		for (ResourceAttribute resource : rbacModel.getResourceAttributes()) {
			BPAttribute bpAttribute = processModel.createOrGetAttribute(resource.getName());
			processModel.addAttribute(bpAttribute);
			
			bpAttribute.setValues(resource.getValues());
			
			for (Operation operation : resource.getOperations()) {
				BPActivity activity = processModel.getActivity(operation.getName());
				bpAttribute.addActivity(activity);
			}
		}
//		
//		System.out.println("debug:");
//		for (BPActivity activity : processModel.getActivities()) {
//			for (BPRole role : activity.getRoles()) {
//				System.out.println(activity.getName() + "\t" + role.getName());
//			}
//		}
		
		return processModel;
	}
	
	public ArrayList<LTLFormula> createLTLFormulasFromRbacModel(RbacModel rbacModel) {
		HashMap<ResourceAttribute, HashMap<Operation, HashSet<Role>>> rolePermissionAssignments = rbacModel.getRolePermissionAssignments();
		ArrayList<LTLFormula> formulas = new ArrayList<LTLFormula>();
		
		HashMap<String, HashSet<String>> operationSubjectAssignments = new HashMap<String, HashSet<String>>();
		
		for (Entry<ResourceAttribute, HashMap<Operation, HashSet<Role>>> entry : rolePermissionAssignments.entrySet()) {
			for (Entry<Operation, HashSet<Role>> subEntry : entry.getValue().entrySet()) {
				
				Operation operation = subEntry.getKey();
				HashSet<Role> roles = subEntry.getValue();
				
				HashSet<String> subjectNames = new HashSet<String>();
				if (operationSubjectAssignments.containsKey(operation.getName())) {
					subjectNames = operationSubjectAssignments.get(operation.getName());
				}
				
				for (Role role : roles) {
					for (Subject subject : role.getSubjects()) {
						subjectNames.add(subject.getName());
					}
				}
				
				operationSubjectAssignments.put(operation.getName(), subjectNames);
			}
		}
		
		for (Entry<String, HashSet<String>> entry : operationSubjectAssignments.entrySet()) {
			String operationName = entry.getKey();
			String[] operationNameParts = operationName.split("\\\\n", 2); // Exclude transition type
			
			String activity = "";
			if (operationNameParts.length > 1) {
				activity = "( activity == \"" + operationNameParts[0] + "\" /\\ eventType == \"" + operationNameParts[1] +"\" )";
			} else {
				activity = "activity == \"" + operationNameParts[0] + "\"";
			}
			
			
			
			String prevSubjectName = null;
			String subjects = null;
			for (String subjectName : entry.getValue()) {
				subjects = "subject == \"" + subjectName + "\"";
				if (prevSubjectName != null) {
					subjects = "( " + subjects + " \\/ " +  prevSubjectName + " )";
				}
				prevSubjectName = subjects;
			}
			
			if (subjects != null) {
				String formulaName = operationName
						.replaceAll("\\\\n", " ")
						.replaceAll(" ", "_")
						.replaceAll("[^a-zA-Z0-9-_]", "");
				String content ="!(<>( ( " + activity + " /\\ !( " + subjects + " ) ) ))";
				
				//String content ="(<>( ( " + activity + " /\\ " + subjects + ") ) \\/ !(<>(" + activity + ")))";
				LTLFormula formula = new LTLFormula("RO_" + formulaName, content);
				formulas.add(formula);
			}
		}
		
		return formulas;
	}
}
