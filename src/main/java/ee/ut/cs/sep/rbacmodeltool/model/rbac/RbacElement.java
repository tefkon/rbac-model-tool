package ee.ut.cs.sep.rbacmodeltool.model.rbac;

abstract public class RbacElement {
	
	protected ElementID id;
	
	protected String name;
	
	protected RbacModel model;
	
	public RbacElement(String name, RbacModel model) {
		this.name = name;
		this.model = model;
		this.id = new ElementID(name);
	}

	public ElementID getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public RbacModel getModel() {
		return model;
	}
	
	public int hashCode() {
		return id.hashCode();
	}
	
	public String toString() {
		return getName();
	}
	
}
