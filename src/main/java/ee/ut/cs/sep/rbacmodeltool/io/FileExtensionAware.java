package ee.ut.cs.sep.rbacmodeltool.io;

public interface FileExtensionAware {

	public String getFileExtension();
	
}
