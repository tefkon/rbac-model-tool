package ee.ut.cs.sep.rbacmodeltool.config;

import java.util.Properties;

public class Config {
	
	private Properties configFile;
	
	private static Config instance;
	
	public Config() {
		configFile = new Properties();
		try {
			configFile.load(this.getClass().getClassLoader()
				.getResourceAsStream("config.properties"));
		} catch (Exception eta) {
			eta.printStackTrace();
		}
	}
	
	private String getValue(String key) {
		
		return configFile.getProperty(key);
	}
	
	public static String getProperty(String key) {
		if (instance == null) {
			instance = new Config();
		}
		
		return instance.getValue(key);
	}
	
	public static Config getInstance() {
		if (instance == null) {
			instance = new Config();
		}
		
		return instance;
	}
}
